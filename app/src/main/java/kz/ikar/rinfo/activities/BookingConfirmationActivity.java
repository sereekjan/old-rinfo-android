package kz.ikar.rinfo.activities;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.widget.Toast;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import kz.ikar.rinfo.R;

public class BookingConfirmationActivity extends AppCompatActivity {
    LinearLayout llBlockCodeConfirmation;
    Button btnOrder;
    EditText etName, etPhone, etConfirmCode;
    Button btnSendAgain;
    ImageView ivBack;

    String confirmCode;
    ParseObject reservationObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirmation);

        ivBack = findViewById(R.id.iv_back);
        llBlockCodeConfirmation = findViewById(R.id.block_code);
        btnOrder = findViewById(R.id.btn_order);
        etName = findViewById(R.id.et_name);
        etPhone = findViewById(R.id.et_phone);
        etConfirmCode = findViewById(R.id.et_confirm_code);
        btnSendAgain = findViewById(R.id.btn_send_again);

        Bitmap back = RestaurantActivity.blurredBackground;
        ivBack.setScaleType(ScaleType.CENTER_CROP);
        ivBack.setImageBitmap(back);

        ParseUser user = ParseUser.getCurrentUser();
        if (user != null) {
            etName.setText(user.getString("name"));
            etPhone.setText(user.getUsername());
        }

        final String comment = getIntent().getStringExtra("Comment");

        int height = getStatusBarHeight();
        View statusBar = findViewById(R.id.status_bar);
        statusBar.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.arrow_left));

        setStatusBarTranslucent(true);

        btnOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etName.getText() == null || etName.getText().toString().equals("")) {
                    Toast.makeText(BookingConfirmationActivity.this, "Введите верное имя",
                        Toast.LENGTH_SHORT)
                        .show();
                    return;
                }

                if (etPhone.getText() == null || etPhone.getText().length() < 11 || etPhone.getText().length() > 12) {
                    Toast.makeText(BookingConfirmationActivity.this, "Введите правильный номер телефона",
                        Toast.LENGTH_SHORT)
                        .show();
                    return;
                }

                if (llBlockCodeConfirmation.getVisibility() == View.VISIBLE) {
                    if (etConfirmCode.getText() == null || etConfirmCode.getText().length() != 6) {
                        Toast.makeText(BookingConfirmationActivity.this,
                            "Введите код смс сообщения", Toast.LENGTH_SHORT)
                            .show();
                        return;
                    }

                    if (!etConfirmCode.getText().toString().equals(reservationObject.getString("code"))) {
                        Toast.makeText(BookingConfirmationActivity.this,
                            "Введите корректный код из смс сообщения", Toast.LENGTH_SHORT)
                            .show();
                        return;
                    }

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                reservationObject.put("verified", true);
                                reservationObject.save();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();

                    Toast.makeText(BookingConfirmationActivity.this,
                                "Заявка отправлена",
                                Toast.LENGTH_SHORT).show();

                    etConfirmCode.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setResult(RESULT_OK);
                            finish();
                        }
                    }, 500);

                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ParseQuery<ParseObject> place = ParseQuery.getQuery("Place");
                                reservationObject = new ParseObject("Reservation");
                                reservationObject.put("user", ParseUser.getCurrentUser());
                                reservationObject.put("place", place.get(getIntent().getStringExtra("RestaurantId")));

                                SimpleDateFormat format = new SimpleDateFormat("HH:mm", new Locale("ru", "RU"));
                                Date pickedDate = (Date) getIntent().getSerializableExtra("Time");
                                reservationObject.put("time", format.format(pickedDate));

                                reservationObject.put("date", pickedDate);
                                reservationObject.put("phone", etPhone.getText().toString());
                                reservationObject.put("numberOfPersons", getIntent().getIntExtra("PeopleCount", 0));
                                reservationObject.put("verified", false);
                                //confirmCode = generateConfirmationCode();
                                reservationObject.put("code", generateConfirmationCode());
                                reservationObject.put("status", "pending");

                                if (comment != null && !comment.equals("")) {
                                    reservationObject.put("comment", comment);
                                }

                                reservationObject.save();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        llBlockCodeConfirmation.setVisibility(View.VISIBLE);
                                        btnOrder.setText("Забронировать");
                                    }
                                });
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }
            }
        });

        btnSendAgain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etName.getText() == null || etName.getText().toString().equals("")) {
                    Toast.makeText(BookingConfirmationActivity.this, "Введите верное имя",
                        Toast.LENGTH_SHORT)
                        .show();
                    return;
                }

                if (etPhone.getText() == null || etPhone.getText().length() < 11 || etPhone.getText().length() > 12) {
                    Toast.makeText(BookingConfirmationActivity.this, "Введите правильный номер телефона",
                        Toast.LENGTH_SHORT)
                        .show();
                    return;
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HashMap<String, Object> params = new HashMap<>();
                        params.put("reserveId", reservationObject.getObjectId());
                        try {
                            ParseCloud.callFunction("sendSMS", params);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                Toast.makeText(BookingConfirmationActivity.this, "Смс повторно отправлено",
                    Toast.LENGTH_SHORT)
                    .show();
            }
        });
    }

    private String generateConfirmationCode() {
        String confirmationCode = "";

        for (int i = 0; i < 6; i++) {
            Random rnd = new Random();
            int num = rnd.nextInt(10);
            confirmationCode = confirmationCode.concat(String.valueOf(num));
        }

        return confirmationCode;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        }else{
            height = 0;
        }

        return height;
    }
}
