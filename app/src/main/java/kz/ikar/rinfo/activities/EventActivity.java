package kz.ikar.rinfo.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.widget.TextView;
import android.widget.Toast;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.ScaleType;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.InteriorAdapter;
import org.json.JSONException;

public class EventActivity extends AppCompatActivity {
    //ImageView ivLogo;
    //TextView tvPhotoCount;
    TextView tvEventType;
    TextView tvEventTitle;
    TextView tvPlaceTitle;
    TextView tvPlaceAddress;
    TextView tvPlaceDistance;
    TextView tvMetro;
    Button btnOnMap;
    TextView tvDescription;
    TextView tvPhone;
    //TextView tvInterior;
    //RecyclerView rvInterior;
    SliderLayout slider;
    Button btnOrder;
    LinearLayout llRestaurant;
    LinearLayout llPhone;
    RelativeLayout rlMain;
    TextView tvNoConnection;
    SwipeRefreshLayout srlUpdate;

    LatLng currentLocation;
    LatLng restLocation;
    Intent bookingIntent;;
    Intent findOnMapIntent;
    private static final int REQUEST_LOCATION = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        //ivLogo = findViewById(R.id.iv_logo);
        //tvPhotoCount = findViewById(R.id.tv_photo_count);
        tvEventType = findViewById(R.id.tv_type);
        tvEventTitle = findViewById(R.id.tv_title);
        tvPlaceTitle = findViewById(R.id.tv_place_title);
        tvPlaceAddress = findViewById(R.id.tv_place_address);
        tvPlaceDistance = findViewById(R.id.tv_place_distance);
        tvMetro = findViewById(R.id.tv_metro);
        btnOnMap = findViewById(R.id.btn_on_map);
        tvDescription = findViewById(R.id.tv_description);
        tvPhone = findViewById(R.id.tv_phone);
        //tvInterior = findViewById(R.id.tv_interior_photo_count);
        //rvInterior = findViewById(R.id.rv_interior);
        btnOrder = findViewById(R.id.btn_order);
        llRestaurant = findViewById(R.id.ll_description);
        llPhone = findViewById(R.id.ll_phone);
        slider = findViewById(R.id.slider);
        rlMain = findViewById(R.id.content);
        tvNoConnection = findViewById(R.id.tv_no_connection);
        srlUpdate = findViewById(R.id.srl_update);

        //rvInterior.setLayoutManager(new GridLayoutManager(this, 1, LinearLayoutManager.HORIZONTAL, false));

        /*int height = getStatusBarHeight();
        View statusBar = findViewById(R.id.status_bar);
        statusBar.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height));*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.arrow_left));

        final String eventId = getIntent().getStringExtra("EventId");

        //setStatusBarTranslucent(true);
        bookingIntent = new Intent(EventActivity.this, BookingActivity.class);
        findOnMapIntent = new Intent(this, FindOnMapActivity.class);

        btnOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ParseUser.getCurrentUser() != null) {
                    startActivity(bookingIntent);
                } else {
                    Toast.makeText(EventActivity.this, "Нобходима авторизация",
                        Toast.LENGTH_SHORT).show();
                }
            }
        });

        srlUpdate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet(EventActivity.this)) {
                    loadEvent(eventId);
                    currentLocation = getLocation();
                    tvNoConnection.setVisibility(View.GONE);
                    rlMain.setVisibility(View.VISIBLE);
                } else {
                    rlMain.setVisibility(View.GONE);
                    tvNoConnection.setVisibility(View.VISIBLE);
                    srlUpdate.setRefreshing(false);
                }
            }
        });

        btnOnMap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (findOnMapIntent != null)
                    startActivity(findOnMapIntent);
            }
        });

        if (isConnectedToInternet(EventActivity.this)) {
            loadEvent(eventId);
            currentLocation = getLocation();
        } else {
            rlMain.setVisibility(View.GONE);
            tvNoConnection.setVisibility(View.VISIBLE);
        }
    }

    public static boolean isConnectedToInternet(Context context) {

        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private LatLng getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
            (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

            return null;
        } else {
            statusCheck();

            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            }

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();

                return new LatLng(latti, longi);
            }

            return null;
        }
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Похоже что GPS выключен, желаете включить?")
            .setCancelable(false)
            .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            })
            .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    dialog.cancel();
                }
            });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults) {

        switch (requestCode) {
            case 8: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    currentLocation = getLocation();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        }else{
            height = 0;
        }

        return height;
    }

    private void loadEvent(String eventId) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.include("place");
        query.whereEqualTo("objectId", eventId);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (object != null) {
                    String type = object.getString("type"),
                        title = object.getString("title"),
                        fullDescription = object.getString("fullDescription"),
                        time = object.getString("dueToTitle"),
                        metro = object.getString("metroDistance");

                    ParseFile file = object.getParseFile("image");

                    final ParseObject place = object.getParseObject("place");

                    if (type != null && title != null && fullDescription != null && file != null && place !=  null) {
                        /*Picasso.with(ivLogo.getContext())
                            .load(object.getParseFile("image").getUrl())
                            .resize(500, 200)
                            .into(ivLogo);*/

                        slider.addSlider(
                            new DefaultSliderView(EventActivity.this)
                                .image(object.getParseFile("image").getUrl())
                                .setScaleType(ScaleType.CenterCrop));

                        getSupportActionBar().setTitle(time);
                        bookingIntent.putExtra("RestaurantId", place.getObjectId());

                        tvEventType.setText(type);
                        tvEventTitle.setText(title);
                        tvPlaceTitle.setText(place.getString("title"));
                        tvPlaceAddress.setText(place.getString("address"));

                        findOnMapIntent.putExtra("Title", place.getString("title"));
                        ParseGeoPoint point = place.getParseGeoPoint("location");
                        findOnMapIntent.putExtra("Lat", point.getLatitude());
                        findOnMapIntent.putExtra("Lon", point.getLongitude());

                        ParseGeoPoint geoLocation = place.getParseGeoPoint("location");
                        if (currentLocation != null) {
                            float[] results = new float[1];
                            Location
                                .distanceBetween(currentLocation.latitude, currentLocation.longitude,
                                    geoLocation.getLatitude(), geoLocation.getLongitude(), results);
                            tvPlaceDistance.setText("" + (int) results[0] + " м.");
                        } else {
                            restLocation = new LatLng(geoLocation.getLatitude(), geoLocation.getLongitude());
                        }

                        if (metro != null && !metro.isEmpty()) {
                            tvMetro.setText(metro);
                        }

                        tvMetro.setText(place.getString("metroDistance"));
                        tvDescription.setText(fullDescription);

                        llRestaurant.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent activityIntent = new Intent(EventActivity.this, RestaurantActivity.class);
                                activityIntent.putExtra("RestaurantId", place.getObjectId());
                                startActivity(activityIntent);
                            }
                        });

                        if (place.getJSONArray("phones") != null && place.getJSONArray("phones").length() != 0) {
                            try {
                                tvPhone.setText(place.getJSONArray("phones").getString(0));
                                llPhone.setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_DIAL);
                                        intent.setData(Uri.parse("tel:" + tvPhone.getText().toString()));
                                        startActivity(intent);
                                    }
                                });
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                        //tvInterior.setText("1");

                        loadInterior(place);

                        srlUpdate.setRefreshing(false);
                        srlUpdate.setEnabled(false);
                    }
                } else {
                    Log.e("RINFO", "Request failed");
                    e.printStackTrace();
                }
            }
        });
    }

    private void loadInterior(ParseObject place) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Gallery");
        query.whereEqualTo("place", place);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    //tvInterior.setText(objects.size() + " фото");
                    //List<String> pics = new ArrayList<String>();
                    for (ParseObject interiorObject : objects) {
                        ParseFile file = interiorObject.getParseFile("image");

                        slider.addSlider(
                            new DefaultSliderView(EventActivity.this)
                                .image(file.getUrl())
                                .setScaleType(ScaleType.CenterCrop));

                        /*String url = file.getUrl();
                        pics.add(url);*/
                    }
                    /*adapter = new InteriorAdapter(pics);
                    rvInterior.setAdapter(adapter);*/
                } else {

                }
            }
        });
    }
}
