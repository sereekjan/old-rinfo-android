package kz.ikar.rinfo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.widget.TextView;
import android.widget.Toast;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.ScaleType;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.InteriorAdapter;
import org.json.JSONException;

public class DiscountActivity extends AppCompatActivity {
    ImageView ivLogo;
    //TextView tvPhotoCount;
    TextView tvEventType;
    TextView tvEventTitle;
    TextView tvRestaurantsInCityCount;
    //Button btnRestaurants;
    TextView tvFullDescription;
    TextView tvRestaurantsCount;
    //TextView tvRestaurantsPhotoCount;
    //RecyclerView rvInterior;
    Button btnOrder;
    LinearLayout llRestaurants;
    //SliderLayout slider;
    RelativeLayout rlMain;
    TextView tvNoConnection;
    SwipeRefreshLayout srlUpdate;

    InteriorAdapter adapter;
    Intent bookingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);

        ivLogo = findViewById(R.id.iv_logo);
        //tvPhotoCount = findViewById(R.id.tv_photo_count);
        tvEventType = findViewById(R.id.tv_event_type);
        tvEventTitle = findViewById(R.id.tv_event_title);
        tvRestaurantsInCityCount = findViewById(R.id.tv_restaurants_in_city);
        //btnRestaurants = findViewById(R.id.btn_restaurants);
        tvFullDescription = findViewById(R.id.tv_full_description);
        tvRestaurantsCount = findViewById(R.id.tv_restaurants_count);
        //tvRestaurantsPhotoCount = findViewById(R.id.tv_restaurants_photo_count);
        //rvInterior = findViewById(R.id.rv_restaurants_photos);
        btnOrder = findViewById(R.id.btn_order);
        llRestaurants = findViewById(R.id.ll_restaurants);
        //slider = findViewById(R.id.slider);
        rlMain = findViewById(R.id.content);
        tvNoConnection = findViewById(R.id.tv_no_connection);
        srlUpdate = findViewById(R.id.srl_update);

        //rvInterior.setLayoutManager(new GridLayoutManager(this, 1, LinearLayoutManager.HORIZONTAL, false));

        /*int height = getStatusBarHeight();
        View statusBar = findViewById(R.id.status_bar);
        statusBar.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height));*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.arrow_left));

        final String discountId = getIntent().getStringExtra("DiscountId");

        bookingIntent = new Intent(DiscountActivity.this, BookingActivity.class);
        //setStatusBarTranslucent(true);

        btnOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ParseUser.getCurrentUser() != null) {
                    startActivity(bookingIntent);
                } else {
                    Toast.makeText(DiscountActivity.this, "Нобходима авторизация",
                        Toast.LENGTH_SHORT).show();
                }
            }
        });

        srlUpdate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet(DiscountActivity.this)) {
                    loadDiscount(discountId);
                    tvNoConnection.setVisibility(View.GONE);
                    rlMain.setVisibility(View.VISIBLE);
                } else {
                    rlMain.setVisibility(View.GONE);
                    tvNoConnection.setVisibility(View.VISIBLE);
                    srlUpdate.setRefreshing(false);
                }
            }
        });

        if (isConnectedToInternet(DiscountActivity.this)) {
            loadDiscount(discountId);
        } else {
            rlMain.setVisibility(View.GONE);
            tvNoConnection.setVisibility(View.VISIBLE);
        }
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private void loadDiscount(String discountId) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("SpecialOffer");
        query.include("place");
        query.include("chain");
        query.whereEqualTo("objectId", discountId);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (object != null) {
                    String title = object.getString("title"),
                        description = object.getString("fullDescription"),
                        city = object.getString("city"),
                        time = object.getString("dueToTitle");

                    ParseFile file = object.getParseFile("image");

                    final ParseObject place = object.getParseObject("place");

                    if (title != null && description != null && city != null && time != null && file != null && place !=  null) {
                        /*Picasso.with(ivLogo.getContext())
                            .load(object.getParseFile("image").getUrl())
                            .resize(400, 200)
                            .into(ivLogo);*/

                        Picasso.with(DiscountActivity.this)
                                .load(file.getUrl())
                                .fit()
                                .centerCrop()
                                .into(ivLogo);

                        /*slider.addSlider(
                            new DefaultSliderView(DiscountActivity.this)
                                .image(object.getParseFile("image").getUrl())
                                .setScaleType(ScaleType.CenterCrop));*/

                        getSupportActionBar().setTitle(time);

                        bookingIntent.putExtra("RestaurantId", place.getObjectId());

                        final ParseObject chainObject = object.getParseObject("chain");
                        if (chainObject != null) {
                            llRestaurants.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent activityIntent = new Intent(DiscountActivity.this, ChainRestaurantsActivity.class);
                                    activityIntent.putExtra("ChainId", chainObject.getObjectId());
                                    startActivity(activityIntent);
                                }
                            });

                            ParseQuery<ParseObject> restObject = ParseQuery.getQuery("Place");
                            restObject.include("chain");

                            int count = 0;

                            try {
                                count = restObject.count();
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }

                            String countStr = count + " ";
                            if (count >= 5 && count <= 20) {
                                countStr += "заведений";
                            } else if (count % 10 >= 2 && count % 10 <= 4) {
                                countStr += "заведения";
                            } else if (count % 10 == 1) {
                                countStr += "заведение";
                            } else {
                                countStr += "заведений";
                            }
                            tvRestaurantsInCityCount.setText(countStr + " в вашем городе");
                            tvRestaurantsCount.setText(count + "");
                        } else {
                            llRestaurants.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent activityIntent = new Intent(DiscountActivity.this, RestaurantActivity.class);
                                    activityIntent.putExtra("RestaurantId", place.getObjectId());
                                    startActivity(activityIntent);
                                }
                            });

                            tvRestaurantsInCityCount.setText("1 заведение в вашем городе");
                            tvRestaurantsCount.setText("1");
                        }

                        /*btnRestaurants.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent activityIntent = new Intent(DiscountActivity.this, RestaurantActivity.class);
                                activityIntent.putExtra("RestaurantId", place.getObjectId());
                                startActivity(activityIntent);
                            }
                        });*/

                        //tvEventType.setText(type);
                        tvEventTitle.setText(title);
                        tvFullDescription.setText(description);

                        //loadInterior(place);

                        srlUpdate.setRefreshing(false);
                        srlUpdate.setEnabled(false);
                    }
                } else {
                    Log.e("RINFO", "Request failed");
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        }else{
            height = 0;
        }

        return height;
    }

    /*private void loadInterior(ParseObject place) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Gallery");
        query.whereEqualTo("place", place);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    //tvRestaurantsPhotoCount.setText(objects.size() + " фото");
                    //List<String> pics = new ArrayList<String>();
                    for (ParseObject interiorObject : objects) {
                        ParseFile file = interiorObject.getParseFile("image");
                        slider.addSlider(
                            new DefaultSliderView(DiscountActivity.this)
                                .image(file.getUrl())
                                .setScaleType(ScaleType.CenterCrop));
                        //String url = file.getUrl();
                        //pics.add(url);
                    }
                    /*adapter = new InteriorAdapter(pics);
                    rvInterior.setAdapter(adapter);
                } else {

                }
            }
        });
    }*/
}
