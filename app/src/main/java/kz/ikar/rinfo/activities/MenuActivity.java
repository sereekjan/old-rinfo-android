package kz.ikar.rinfo.activities;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.github.barteksc.pdfviewer.PDFView;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.helpers.TouchImageView;

public class MenuActivity extends AppCompatActivity {
    PDFView pdfView;
    TouchImageView ivMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        pdfView = findViewById(R.id.pdfView);
        ivMenu = findViewById(R.id.iv_file);

        final String fileUrl = getIntent().getStringExtra("FileUrl");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Меню");
        actionBar.setDisplayHomeAsUpEnabled(true);

        String format = fileUrl.substring(fileUrl.lastIndexOf(".") + 1);
        if (format.equals("pdf")) {
            ivMenu.setVisibility(View.GONE);
            pdfView.setVisibility(View.VISIBLE);

            new DownloadFile().execute(fileUrl, "temp.pdf", this.getCacheDir().getAbsolutePath());
        } else {
            pdfView.setVisibility(View.GONE);
            ivMenu.setVisibility(View.VISIBLE);

            Picasso.with(this)
                .load(fileUrl)
                .into(ivMenu);
        }
        //pdfView.fromUri(Uri.parse(fileUrl)).load();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    class DownloadFile extends AsyncTask<String, Void, Void> {

        private File file;

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = strings[2];
            File folder = new File(extStorageDirectory, "rinfo");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            downloadFile(fileUrl, pdfFile);
            file = pdfFile;
            long len = file.length();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pdfView.fromFile(file).load();
        }

        private static final int  MEGABYTE = 1024 * 1024;

        public void downloadFile(String fileUrl, File directory){
            try {

                URL url = new URL(fileUrl);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(directory);
                int totalSize = urlConnection.getContentLength();

                byte[] buffer = new byte[MEGABYTE];
                int bufferLength = 0;
                while((bufferLength = inputStream.read(buffer))>0 ){
                    fileOutputStream.write(buffer, 0, bufferLength);
                }
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


