package kz.ikar.rinfo.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.TabViewPagerAdapter;
import kz.ikar.rinfo.welcomescreen.WelcomeFragment;

public class SplashActivity extends AppCompatActivity {
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        viewPager = findViewById(R.id.view_pager);

        setStatusBarTranslucent(true);

        setupViewPager();
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (makeTranslucent) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            } else {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
    }

    private void moveToApp() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveWatchedState() {
        SharedPreferences sPrefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        sPrefs.edit().putBoolean("IsWatched", true).apply();
    }

    private void setupViewPager() {
        View.OnClickListener nextListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() == 3) {
                    saveWatchedState();
                    moveToApp();
                } else {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                }
            }
        };
        View.OnClickListener skipListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveWatchedState();
                moveToApp();
            }
        };
        TabViewPagerAdapter adapter = new TabViewPagerAdapter(getSupportFragmentManager());

        WelcomeFragment welcomeFragmentFirst = new WelcomeFragment();
        welcomeFragmentFirst.setImageDrawable(R.mipmap.bottom_menu_example);
        welcomeFragmentFirst.setDescription(getString(R.string.slider_first));
        welcomeFragmentFirst.setBtnNextListener(nextListener);
        welcomeFragmentFirst.setBtnSkipListener(skipListener);

        WelcomeFragment welcomeFragmentSecond = new WelcomeFragment();
        welcomeFragmentSecond.setImageDrawable(R.mipmap.cousines_example);
        welcomeFragmentSecond.setDescription(getString(R.string.slider_second));
        welcomeFragmentSecond.setBtnNextListener(nextListener);
        welcomeFragmentSecond.setBtnSkipListener(skipListener);

        WelcomeFragment welcomeFragmentThird = new WelcomeFragment();
        welcomeFragmentThird.setImageDrawable(R.mipmap.emojis);
        welcomeFragmentThird.setDescription(getString(R.string.slider_third));
        welcomeFragmentThird.setBtnNextListener(nextListener);
        welcomeFragmentThird.setBtnSkipListener(skipListener);

        WelcomeFragment welcomeFragmentForth = new WelcomeFragment();
        welcomeFragmentForth.setImageDrawable(R.mipmap.book_example);
        welcomeFragmentForth.setDescription(getString(R.string.slider_forth));
        welcomeFragmentForth.setBtnNextListener(nextListener);
        welcomeFragmentForth.setBtnSkipListener(skipListener);

        adapter.addFragment(welcomeFragmentFirst, "rinfo");
        adapter.addFragment(welcomeFragmentSecond, "rinfo");
        adapter.addFragment(welcomeFragmentThird, "rinfo");
        adapter.addFragment(welcomeFragmentForth, "rinfo");
        viewPager.setAdapter(adapter);
    }


}
