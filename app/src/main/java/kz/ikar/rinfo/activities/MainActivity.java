package kz.ikar.rinfo.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.Toolbar;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;

import java.util.HashMap;
import java.util.Map;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.ViewPagerAdapter;
import kz.ikar.rinfo.fragments.DiscountsFragment;
import kz.ikar.rinfo.fragments.ProfileFragment;
import kz.ikar.rinfo.fragments.RestaurantsFragment;
import kz.ikar.rinfo.fragments.profile.ProfileChangeFragment;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    ViewPager viewPager;

    public boolean isReadyPressed = false;
    public static Bitmap firstPhoto;
    ViewPager.OnPageChangeListener onPageChangeListener;

    private boolean isChangingState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.bottomNavView);
        viewPager = findViewById(R.id.view_pager);

        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        bottomNavigationView.setSelectedItemId(R.id.action_discounts);
                        break;
                    case 1:
                        bottomNavigationView.setSelectedItemId(R.id.action_restaurants);
                        break;
                    case 2:
                        bottomNavigationView.setSelectedItemId(R.id.action_profile);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //Toast.makeText(MainActivity.this, "" + state, Toast.LENGTH_SHORT).show();
            }
        };

        /*final TabLayout toolbar1 = findViewById(R.id.toolbar1);
        final TabLayout toolbar2 = findViewById(R.id.toolbar2);*/

        /*int height = getStatusBarHeight();
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) toolbar1.getLayoutParams();
        params.topMargin = height;
        params = (ViewGroup.MarginLayoutParams) toolbar2.getLayoutParams();
        params.topMargin = height;*/

        setupViewPager();
        viewPager.addOnPageChangeListener(onPageChangeListener);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (bottomNavigationView.getSelectedItemId() != item.getItemId()) {
                    //boolean result = bottomNavigationItemSelect(item.getItemId());
                    //viewPager.clearOnPageChangeListeners();
                    isChangingState = true;
                    switch (item.getItemId()) {
                        case R.id.action_discounts:
                            viewPager.setCurrentItem(0);
                            break;
                        case R.id.action_restaurants:
                            viewPager.setCurrentItem(1);
                            break;
                        case R.id.action_profile:
                            viewPager.setCurrentItem(2);
                            break;
                    }
                    //viewPager.addOnPageChangeListener(onPageChangeListener);

                    /*toolbar1.setVisibility(View.GONE);
                    toolbar2.setVisibility(View.GONE);
                    if (item.getItemId() == R.id.action_discounts) {
                        toolbar1.setVisibility(View.VISIBLE);
                    } else if (item.getItemId() == R.id.action_restaurants) {
                        toolbar2.setVisibility(View.VISIBLE);
                    }*/

                    return true;
                }
                return false;
            }
        });

        bottomNavigationView.setSelectedItemId(R.id.action_restaurants);

        setStatusBarTranslucent(true);

        /*try {
            ParseInstallation.getCurrentInstallation().delete();
            ParseInstallation.getCurrentInstallation().save();
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        //Toast.makeText(this, ParseInstallation.getCurrentInstallation().getString("deviceToken"), Toast.LENGTH_SHORT).show();
        //ParseInstallation.getCurrentInstallation().put("GCMSenderId", "id:308829784283");
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        if (ParseUser.getCurrentUser() != null)
            installation.put("user", ParseUser.getCurrentUser());
        installation.put("GCMSenderId", "id:308829784283");
        installation.saveInBackground();
        //Toast.makeText(this, ParseInstallation.getCurrentInstallation().getString("GCMSenderId"), Toast.LENGTH_SHORT).show();
        ParsePush.subscribeInBackground("global");

        //Log.e("Rinfo", ParseInstallation.getCurrentInstallation()0;
    }

    private void setupViewPager() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new DiscountsFragment());
        viewPagerAdapter.addFragment(new RestaurantsFragment());
        viewPagerAdapter.addFragment(new ProfileFragment());
        viewPager.setAdapter(viewPagerAdapter);
    }

    /*private boolean bottomNavigationItemSelect(int itemId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = null;

        switch (itemId) {
            case R.id.action_discounts:
                fragment = new DiscountsFragment();
                break;
            case R.id.action_restaurants:
                fragment = new RestaurantsFragment();
                break;
            case R.id.action_profile:
                fragment = new ProfileFragment();
                break;
        }

        if (fragment == null) {
            return false;
        }

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        /*Fragment currentFragment = getSupportFragmentManager().findFragmentByTag("CurrentFragment");
        if (currentFragment != null)
            transaction.remove(currentFragment);
        transaction.replace(R.id.content_main, fragment, "CurrentFragment");
        //transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        //transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.commit();

        return true;
    }*/

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (makeTranslucent) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            } else {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
    }

    public int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        }else{
            height = 0;
        }

        return height;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("CurrentFragment");
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("CurrentFragment");
        if (fragment != null) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        /*if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(getApplicationContext(), "Нажмите снова для выхода", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);*/
        super.onBackPressed();

        final Fragment fragment = getSupportFragmentManager().findFragmentByTag("CurrentFragment");
        if (fragment != null && fragment instanceof ProfileFragment) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        ((ProfileFragment)fragment).loadUser();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
