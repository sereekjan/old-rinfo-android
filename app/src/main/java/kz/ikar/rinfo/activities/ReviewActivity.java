package kz.ikar.rinfo.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import kz.ikar.rinfo.R;

public class ReviewActivity extends AppCompatActivity {
    ImageButton ibUp;
    ImageButton ibDown;
    EditText etText;
    Button btnReview;
    ImageView ivBack;

    String restId;

    int thumbState = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Оставить отзыв");
        actionBar.setDisplayHomeAsUpEnabled(true);

        ibUp = findViewById(R.id.ib_up);
        ibDown = findViewById(R.id.ib_down);
        etText = findViewById(R.id.et_text);
        btnReview = findViewById(R.id.btn_review);
        ivBack = findViewById(R.id.iv_back);

        Bitmap back = RestaurantActivity.blurredBackground;
        ivBack.setScaleType(ScaleType.CENTER_CROP);
        ivBack.setImageBitmap(back);

        restId = getIntent().getStringExtra("RestaurantId");

        ibUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                thumbState = 1;
                ibDown.setBackgroundColor(ContextCompat.getColor(ReviewActivity.this, android.R.color.white));
                ibUp.setBackgroundColor(ContextCompat.getColor(ReviewActivity.this, android.R.color.darker_gray));
            }
        });

        ibDown.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                thumbState = 0;
                ibUp.setBackgroundColor(ContextCompat.getColor(ReviewActivity.this, android.R.color.white));
                ibDown.setBackgroundColor(ContextCompat.getColor(ReviewActivity.this, android.R.color.darker_gray));
            }
        });

        btnReview.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (thumbState == -1) {
                    Toast
                        .makeText(ReviewActivity.this, "Выберите тип отзыва", Toast.LENGTH_SHORT)
                        .show();
                    return;
                }

                if (etText.getText() == null || etText.getText().length() < 5) {
                    Toast
                        .makeText(ReviewActivity.this, "Слишком короткий отзыв", Toast.LENGTH_SHORT)
                        .show();
                    return;
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            saveReview();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private void saveReview() throws ParseException {
        ParseQuery<ParseObject> parseQueryPlace = ParseQuery.getQuery("Place");
        ParseObject placeObject = parseQueryPlace.get(restId);
        ParseObject feedbackObject = new ParseObject("Feedback");
        feedbackObject.put("text", etText.getText().toString());
        feedbackObject.put("isPositive", thumbState == 1);
        feedbackObject.put("user", ParseUser.getCurrentUser());
        feedbackObject.put("place", placeObject);
        feedbackObject.save();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    private String getPhoneNumber() {
        SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sPrefs.getString("PhoneNumber", null);
    }
}
