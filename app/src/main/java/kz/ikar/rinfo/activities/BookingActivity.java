package kz.ikar.rinfo.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.TimeAdapter;
import kz.ikar.rinfo.adapters.TimeAdapter.OnItemEventListener;

public class BookingActivity extends AppCompatActivity {

    List<String> time = Arrays.asList(
        "12:00", "12:30", "13:00", "13:30",
        "14:00", "14:30", "15:00", "15:30",
        "16:00", "16:30", "17:00", "17:30",
        "18:00", "18:30", "19:00", "19:30",
        "20:00", "20:30", "21:00", "21:30",
        "22:00", "22:30", "23:00"
    );

    List<String> people = Arrays.asList(
        "1 человек", "2 человека", "3 человека",
        "4 человека", "5 человек", "6 человек",
        "7 человек", "8 человек", "9 человек",
        "10 человек", "11 человек", "12 человек",
        "13 человек", "14 человек", "15 человек"
    );

    RecyclerView rvTime;
    ImageView ivback;
    Button btnDate;
    Button btnPeople;
    Button btnOrder;
    EditText etComment;

    int peopleCount = 1;

    Calendar pickedDate = Calendar.getInstance();
    TimeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        ivback = findViewById(R.id.iv_back);
        rvTime = findViewById(R.id.rv_time);
        btnDate = findViewById(R.id.btn_date);
        //sPeople = findViewById(R.id.spinner_people);
        btnPeople = findViewById(R.id.btn_people);
        btnOrder = findViewById(R.id.btn_order);
        etComment = findViewById(R.id.et_comment);

        Bitmap back = RestaurantActivity.blurredBackground;
        ivback.setScaleType(ScaleType.CENTER_CROP);
        ivback.setImageBitmap(back);

        rvTime.setLayoutManager(new GridLayoutManager(this, 5));

        int height = getStatusBarHeight();
        View statusBar = findViewById(R.id.status_bar);
        statusBar.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.arrow_left));

        Date date = pickedDate.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EE, d MMM", new Locale("ru", "RU"));
        String dateText = dateFormat.format(date);
        btnDate.setText(dateText);

        setStatusBarTranslucent(true);

        adapter = new TimeAdapter(time, new OnItemEventListener() {
            @Override
            public void onItemClicked(String key) {
                if (adapter != null) {
                    if (adapter.indexSelected != -1) {
                        pickedDate.set(Calendar.HOUR_OF_DAY, Integer.parseInt(key.substring(0, 2)));
                        pickedDate.set(Calendar.MINUTE, Integer.parseInt(key.substring(3, 5)));
                    } else {
                        pickedDate.set(Calendar.HOUR_OF_DAY, 0);
                        pickedDate.set(Calendar.MINUTE, 0);
                    }
                    pickedDate.set(Calendar.SECOND, 0);
                }
            }
        });
        rvTime.setAdapter(adapter);

        btnDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatePickerDialog dialog = new DatePickerDialog(btnDate.getContext(),
                    new OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month,
                            int dayOfMonth) {

                        }
                    }, pickedDate.get(Calendar.YEAR), pickedDate.get(Calendar.MONTH), pickedDate.get(Calendar.DAY_OF_MONTH));

                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "ОК",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog2, int which) {
                            DatePicker picker = dialog.getDatePicker();
                            pickedDate.set(picker.getYear(), picker.getMonth(), picker.getDayOfMonth());

                            Date date = pickedDate.getTime();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("EE, d MMM", new Locale("ru", "RU"));
                            String dateText = dateFormat.format(date);
                            btnDate.setText(dateText);
                        }
                    });
                dialog.show();
            }
        });

        btnPeople.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(BookingActivity.this);
                LayoutInflater inflater = getLayoutInflater();

                View rootView = inflater.inflate(R.layout.dialog_people, null);
                final SeekBar seekBar = rootView.findViewById(R.id.seekbar);
                final TextView tvCount = rootView.findViewById(R.id.tv_count);

                seekBar.setMax(14);
                seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        tvCount.setText(people.get(progress));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                builder.setView(rootView)
                    .setTitle("Сколько вас")
                    .setPositiveButton("ОК", null)
                    .setNegativeButton("Отмена", null);

                final AlertDialog alertDialog = builder.create();
                alertDialog.setOnShowListener(new OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        final Button btnOk = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        Button btnCancel = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

                        btnOk.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                peopleCount = seekBar.getProgress() + 1;
                                btnPeople.setText(people.get(peopleCount - 1));
                                alertDialog.dismiss();
                            }
                        });

                        btnCancel.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });
                    }
                });
                alertDialog.show();
            }
        });

        btnOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter.indexSelected == -1) {
                    Toast.makeText(BookingActivity.this, "Выберите время", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent confirmationIntent = new Intent(BookingActivity.this, BookingConfirmationActivity.class);
                confirmationIntent.putExtra("RestaurantId", getIntent().getStringExtra("RestaurantId"));
                confirmationIntent.putExtra("Time", pickedDate.getTime());
                confirmationIntent.putExtra("PeopleCount", peopleCount);
                if (etComment.getText() != null)
                    confirmationIntent.putExtra("Comment", etComment.getText().toString());
                startActivityForResult(confirmationIntent, 12);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 12) {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        }else{
            height = 0;
        }

        return height;
    }
}
