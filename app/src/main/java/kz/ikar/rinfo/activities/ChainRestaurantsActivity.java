package kz.ikar.rinfo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import java.util.ArrayList;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.RestaurantsListAdapter;
import kz.ikar.rinfo.adapters.RestaurantsListAdapter.OnItemEventListener;
import kz.ikar.rinfo.models.Restaurant;

public class ChainRestaurantsActivity extends AppCompatActivity {

    RecyclerView rvRestaurants;
    TextView tvCompTitle;
    TextView tvEmpty;

    public static final String TAG = "RINFO";

    private RestaurantsListAdapter adapter;
    int selectedPlace = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compilation);

        rvRestaurants = findViewById(R.id.rv_restaurants);
        tvCompTitle = findViewById(R.id.tv_compilation_title);
        tvEmpty = findViewById(R.id.tv_empty);

        final GridLayoutManager manager = new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false);
        rvRestaurants.setLayoutManager(manager);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.mipmap.arrow_left_black));

        final String chainId = getIntent().getStringExtra("ChainId");

        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Restaurant> list = getRestaurants(chainId);

                new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        adapter = new RestaurantsListAdapter(list, new OnItemEventListener() {
                            @Override
                            public void onBottomReached(int position) {
                                adapter.datasetChanged(getRestaurants(chainId));
                            }

                            @Override
                            public void onItemClicked(String restaurantId, int position) {
                                selectedPlace = position;
                                Intent activityIntent = new Intent(ChainRestaurantsActivity.this, RestaurantActivity.class);
                                activityIntent.putExtra("RestaurantId", restaurantId);
                                startActivityForResult(activityIntent, 1);
                            }

                            @Override
                            public void onItemFabClicked(final Restaurant restaurant,
                                FloatingActionButton fabLike, TextView tvLikes) {

                                ParseUser currentUser = ParseUser.getCurrentUser();
                                if (currentUser == null) {
                                    Toast
                                        .makeText(ChainRestaurantsActivity.this,
                                            "Необходимо авторизоваться", Toast.LENGTH_SHORT)
                                        .show();
                                    return;
                                }

                                if (!restaurant.isFavorite()) {
                                    fabLike.setImageDrawable(
                                        ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.favorite_filled));
                                    int count = Integer.parseInt(tvLikes.getText().toString()) + 1;
                                    tvLikes.setText("" + count);
                                } else {
                                    fabLike.setImageDrawable(ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.like));
                                    int count = Integer.parseInt(tvLikes.getText().toString()) - 1;
                                    tvLikes.setText("" + count);
                                }
                                restaurant.setFavorite(!restaurant.isFavorite());

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        try {
                                            likeRestaurant(restaurant.getId());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }).start();
                            }
                        });
                        if (adapter.getItemCount() == 0) {
                            tvEmpty.setVisibility(View.VISIBLE);
                            rvRestaurants.setVisibility(View.GONE);
                        } else {
                            tvEmpty.setVisibility(View.GONE);
                            rvRestaurants.setVisibility(View.VISIBLE);
                            rvRestaurants.setAdapter(adapter);
                        }
                    }
                });
            }
        }).start();
    }

    private boolean likeRestaurant(String restId) throws ParseException {
        ParseObject restObject = ParseQuery.getQuery("Place").get(restId);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Favorite");
        query.whereEqualTo("place", restObject);
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        int result = query.count();

        if (result != 0) {
            query.getFirst().delete();

            return false;
        } else {
            ParseObject favorite = new ParseObject("Favorite");
            favorite.put("user", ParseUser.getCurrentUser());
            favorite.put("place", restObject);
            favorite.save();

            return true;
        }
    }

    private List<Restaurant> getRestaurants(String chainId) {
        ParseQuery<ParseObject> chainQuery = ParseQuery.getQuery("Chain");
        ParseObject chainObject = null;
        try {
            chainObject = chainQuery.get(chainId);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (chainObject == null) {
            return null;
        }

        ParseQuery<ParseObject> restQuery = ParseQuery.getQuery("Place");
        restQuery.include("chain");
        restQuery.whereEqualTo("chain", chainObject);

        final ParseObject finalChainObject = chainObject;
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                tvCompTitle.setText(finalChainObject.getString("title"));
            }
        });

        List<Restaurant> restaurants = new ArrayList<>();
        restQuery.orderByAscending("order");
        restQuery.setLimit(10);

        int skipCount = 10 * (adapter == null ? 0 : ((adapter.getItemCount() + 9) / 10));
        restQuery.setSkip(skipCount);

        List<ParseObject> objects = null;
        try {
            objects = restQuery.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (objects != null) {
            for (ParseObject place : objects) {
                if (place != null) {
                    String address = place.getString("address"),
                        avgCost = "Ср. чек: " + place.get("averageBill") + " ₸",
                        title = place.getString("title"),
                        synopsis = place.getString("synopsis"),
                        shortDesc = place.getString("shortdesc");

                    int favCount = place.getInt("favoriteCount");

                    ParseFile file = place.getParseFile("image");

                    if (address != null && title != null && synopsis != null && shortDesc != null
                        && file != null) {
                        Restaurant restaurant = new Restaurant(
                            place.getObjectId(),
                            address,
                            avgCost,
                            title,
                            synopsis,
                            shortDesc,
                            favCount,
                            file.getUrl(),
                            place.getString("discountTitle"),
                            place.getString("discountDuration")
                        );

                        if (ParseUser.getCurrentUser() != null) {
                            ParseQuery<ParseObject> likeQuery = ParseQuery.getQuery("Favorite");
                            likeQuery.whereEqualTo("place", place);
                            likeQuery.whereEqualTo("user", ParseUser.getCurrentUser());
                            try {
                                restaurant.setFavorite(likeQuery.count() != 0);
                            } catch (ParseException e) {
                                e.printStackTrace();
                                restaurant.setFavorite(false);
                            }
                        }
                        restaurants.add(restaurant);
                    }
                }
            }
        }

        return restaurants;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 7 && selectedPlace != -1) {
            adapter.getmItemList()
                    .get(selectedPlace)
                    .setFavorite(
                            !adapter.getmItemList()
                                    .get(selectedPlace)
                                    .isFavorite()
                    );
            adapter.notifyItemChanged(selectedPlace);

            selectedPlace = -1;
        }
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 7 && selectedPlace != -1) {
            adapter.getmItemList()
                    .get(selectedPlace)
                    .setFavorite(
                            !adapter.getmItemList()
                                    .get(selectedPlace)
                                    .isFavorite()
                    );
            adapter.notifyItemChanged(selectedPlace);

            selectedPlace = -1;
        }
    }*/
}
