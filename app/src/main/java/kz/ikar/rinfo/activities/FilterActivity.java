package kz.ikar.rinfo.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Arrays;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.FilterPricesAdapter;
import kz.ikar.rinfo.adapters.FilterPricesAdapter.OnItemEventListener;
import kz.ikar.rinfo.fragments.restaurants.CuisineFeaturesFragment;
import kz.ikar.rinfo.fragments.restaurants.CuisineNationsFragment;
import kz.ikar.rinfo.models.Cuisine;

public class FilterActivity extends AppCompatActivity {
    RecyclerView rvPrices;
    LinearLayout llFeatures;
    LinearLayout llNations;
    Button btnReady;
    TextView tvFeatures;
    TextView tvCuisines;
    Switch switchOpen;

    FilterPricesAdapter adapter;
    Intent selectedPriceIntent;
    public static boolean isOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        llFeatures = findViewById(R.id.ll_features);
        llNations = findViewById(R.id.ll_nations);
        tvFeatures = findViewById(R.id.tv_features);
        tvCuisines = findViewById(R.id.tv_cuisines);
        switchOpen = findViewById(R.id.switch_open);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnReady = findViewById(R.id.btn_ready);
        selectedPriceIntent = new Intent();
        btnReady.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isOpen = switchOpen.isChecked();
                selectedPriceIntent.putExtra("SelectedValue", adapter.indexSelected);
                setResult(RESULT_OK, selectedPriceIntent);
                finish();
            }
        });

        rvPrices = findViewById(R.id.rv_prices);
        rvPrices.setLayoutManager(new GridLayoutManager(this, 1, LinearLayoutManager.HORIZONTAL, false));

        adapter = new FilterPricesAdapter(
            Arrays.asList("до 5000 ₸", "5000 - 10 000 ₸", "от 10 000 ₸"),
            new OnItemEventListener() {
                @Override
                public void onItemClicked(String key) {

                }
            });
        adapter.indexSelected = getIntent().getIntExtra("SelectedIndex", -1);
        rvPrices.setAdapter(adapter);

        switchOpen.setChecked(isOpen);
        switchOpen.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectedPriceIntent.putExtra("IsOpen", isOpen);
            }
        });

        llFeatures.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_filter, new CuisineFeaturesFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        llNations.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_filter, new CuisineNationsFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    public int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
            "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        }else{
            height = 0;
        }

        return height;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getSupportFragmentManager().findFragmentByTag("CurrentFragment") != null) {
            tvFeatures.setText(Cuisine.GET_SOME_FEATURES_TITLES());
            tvCuisines.setText(Cuisine.GET_SOME_NATIONS_TITLES());
        }
    }
}
