package kz.ikar.rinfo.activities;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.widget.TextView;
import android.widget.Toast;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.ScaleType;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.CuisineAdapter;
import kz.ikar.rinfo.adapters.ReviewsAdapter;
import kz.ikar.rinfo.models.Cuisine;
import kz.ikar.rinfo.models.Review;
import org.json.JSONArray;
import org.json.JSONException;

public class RestaurantActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION = 10;
    FloatingActionButton fabLike;
    ImageView ivHeader;
    TextView tvPlaceTitle;
    TextView tvPlaceAddress;
    TextView tvPlaceDistance;
    TextView tvMetro;
    Button btnOnMap;
    TextView tvWorkTime;
    RecyclerView rvTypes;
    TextView tvDescription;
    TextView tvDescriptionMore;
    TextView tvPhone;
    RecyclerView rvReviews;
    Button btnReview;
    Button btnOrder;
    ImageView ivMetro;
    SliderLayout slider;
    LinearLayout llMainMenu;
    LinearLayout llDrinksMenu;
    LinearLayout llPhone;
    RelativeLayout rlMain;
    TextView tvNoConnection;
    SwipeRefreshLayout srlUpdate;
    TextView tvStatus;

    ReviewsAdapter reviewsAdapter;
    Intent findOnMapIntent;
    Intent reviewIntent;
    FusedLocationProviderClient mFusedLocationClient;
    LatLng currentLocation;
    LatLng restLocation;
    boolean isLiked = false;
    boolean wasLiked = false;
    public static Bitmap blurredBackground;
    //public List<Bitmap> bmps;
    public static List<String> bmps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        fabLike = findViewById(R.id.fab_like);
        ivHeader = findViewById(R.id.iv_header);
        tvPlaceTitle = findViewById(R.id.tv_place_title);
        tvPlaceAddress = findViewById(R.id.tv_place_address);
        tvPlaceDistance = findViewById(R.id.tv_place_distance);
        tvMetro = findViewById(R.id.tv_metro);
        btnOnMap = findViewById(R.id.btn_on_map);
        tvWorkTime = findViewById(R.id.tv_work_time);
        rvTypes = findViewById(R.id.rv_types);
        tvDescription = findViewById(R.id.tv_description);
        tvDescriptionMore = findViewById(R.id.tv_more);
        tvPhone = findViewById(R.id.tv_phone);
        rvReviews = findViewById(R.id.rv_reviews);
        btnReview = findViewById(R.id.btn_review);
        btnOrder = findViewById(R.id.btn_order);
        ivMetro = findViewById(R.id.iv_metro);
        slider = findViewById(R.id.slider);
        llMainMenu = findViewById(R.id.ll_main_menu);
        llDrinksMenu = findViewById(R.id.ll_drinks_menu);
        llPhone = findViewById(R.id.ll_phone);
        rlMain = findViewById(R.id.content);
        tvNoConnection = findViewById(R.id.tv_no_connection);
        srlUpdate = findViewById(R.id.srl_update);
        tvStatus = findViewById(R.id.tv_open_time);

        rvTypes.setLayoutManager(new GridLayoutManager(this, 1, LinearLayoutManager.HORIZONTAL, false));
        rvReviews.setLayoutManager(new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false));
        rvTypes.setNestedScrollingEnabled(false);

        /*int height = getStatusBarHeight();
        View statusBar = findViewById(R.id.status_bar);
        statusBar.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height));*/

        //setStatusBarTranslucent(true);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.mipmap.arrow_left));

        slider.stopAutoCycle();
        bmps = new ArrayList<>();

        if (MainActivity.firstPhoto != null)
            ivHeader.setImageBitmap(MainActivity.firstPhoto);

        btnReview.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ParseUser.getCurrentUser() != null) {
                    if (reviewIntent != null)
                        startActivityForResult(reviewIntent, 11);
                } else {
                    Toast.makeText(RestaurantActivity.this, "Необходимо войти в свой профиль", Toast.LENGTH_SHORT).show();
                }
            }
        });

        llMainMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RestaurantActivity.this, "Меню отсутсвует", Toast.LENGTH_SHORT).show();
            }
        });

        llDrinksMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RestaurantActivity.this, "Меню отсутсвует", Toast.LENGTH_SHORT).show();
            }
        });

        llPhone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + tvPhone.getText().toString()));
                startActivity(intent);
            }
        });

        final String restaurantId = getIntent().getStringExtra("RestaurantId");
        findOnMapIntent = new Intent(this, FindOnMapActivity.class);

        btnOnMap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (findOnMapIntent != null)
                    startActivity(findOnMapIntent);
            }
        });

        btnOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ParseUser.getCurrentUser() != null) {
                    if (ParseUser.getCurrentUser().get("name") == null) {
                        Toast.makeText(RestaurantActivity.this, "Необходимо отредактировать имя профиля",
                            Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Intent bookingIntent = new Intent(RestaurantActivity.this,
                        BookingActivity.class);
                    bookingIntent.putExtra("RestaurantId", restaurantId);
                    /*if (blurredBackground != null) {
                        bookingIntent.putExtra("BackgroundBitmap", blurredBackground);
                    }*/
                    startActivity(bookingIntent);
                } else {
                    Toast.makeText(RestaurantActivity.this, "Нобходима авторизация",
                        Toast.LENGTH_SHORT).show();
                }
            }
        });

        fabLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isLiked = !isLiked;
                changeFabState();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            likeRestaurant(restaurantId);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });

        srlUpdate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet(RestaurantActivity.this)) {
                    loadRestaurant(restaurantId);
                    currentLocation = getLocation();
                    tvNoConnection.setVisibility(View.GONE);
                    rlMain.setVisibility(View.VISIBLE);
                } else {
                    rlMain.setVisibility(View.GONE);
                    tvNoConnection.setVisibility(View.VISIBLE);
                    srlUpdate.setRefreshing(false);
                }
            }
        });

        if (isConnectedToInternet(RestaurantActivity.this)) {
            loadRestaurant(restaurantId);
            currentLocation = getLocation();
        } else {
            rlMain.setVisibility(View.GONE);
            tvNoConnection.setVisibility(View.VISIBLE);
        }
    }

    public static boolean isConnectedToInternet(Context context) {

        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private boolean likeRestaurant(String restId) throws ParseException {
        ParseObject restObject = ParseQuery.getQuery("Place").get(restId);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Favorite");
        query.whereEqualTo("place", restObject);
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        int result = query.count();

        if (result != 0) {
            //if (!isLiked) {
                query.getFirst().delete();
            //}

            return false;
        } else {
            //if (isLiked) {
                ParseObject favorite = new ParseObject("Favorite");
                favorite.put("user", ParseUser.getCurrentUser());
                favorite.put("place", restObject);
                favorite.save();
            //}

            return true;
        }
    }

    private LatLng getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
            (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

            return null;
        } else {
            statusCheck();

            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            }

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();

                return new LatLng(latti, longi);
            }

            return null;
        }
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Похоже что GPS выключен, желаете включить?")
            .setCancelable(false)
            .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            })
            .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    dialog.cancel();
                }
            });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults) {

        switch (requestCode) {
            case 8: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    currentLocation = getLocation();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 11) {
                Toast.makeText(this, "Комментарий отправлен", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void changeFabState() {
        if (isLiked) {
            fabLike.setImageDrawable(ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.favorite_filled));
        } else {
            fabLike.setImageDrawable(ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.like));
        }
    }

    private void loadRestaurant(final String restaurantId) {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Place");
        query.include("schedule");
        reviewIntent = new Intent(RestaurantActivity.this, ReviewActivity.class);
        reviewIntent.putExtra("RestaurantId", restaurantId);
        query.getInBackground(restaurantId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    ParseFile file = object.getParseFile("image");
                    if (file != null) {
                        /*file.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {
                                blurredBackground = BitmapFactory.decodeByteArray(data, 0, data.length);
                                bmps.add(blurredBackground);
                            }
                        });*/
                        bmps.add(file.getUrl());
                        slider.addSlider(
                            new DefaultSliderView(RestaurantActivity.this)
                                .image(file.getUrl())
                                .setScaleType(ScaleType.CenterCrop)
                                .setOnSliderClickListener(
                                new OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {
                                        Intent intent = new Intent(RestaurantActivity.this, SliderActivity.class);
                                        startActivity(intent);
                                        /*DisplayMetrics metrics = getResources().getDisplayMetrics();
                                        ObjectAnimator translationY = ObjectAnimator.ofFloat(
                                                slider.getView(), "y",
                                                metrics.heightPixels / 2 -
                                                        slider.getView().getHeight() / 2
                                        );
                                        translationY.setDuration(500);
                                        translationY.start();*/
                                    }
                                }));
                        ivHeader.setVisibility(View.GONE);
                    }

                    ParseObject scheduleObject = object.getParseObject("schedule");

                    if (scheduleObject != null) {
                        Hashtable.Entry<Boolean, String> status = getWorkingTime(scheduleObject);
                        tvStatus.setText(status.getValue());
                        Drawable img = ContextCompat.getDrawable(RestaurantActivity.this,
                            status.getKey() ? R.drawable.status_open : R.drawable.status_close);
                        tvStatus.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    }

                    JSONArray phones = object.getJSONArray("phones");
                    if (phones != null && phones.length() != 0) {
                        try {
                            String phone = phones.getString(0);
                            tvPhone.setText(phone);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }

                    ParseGeoPoint geoLocation = object.getParseGeoPoint("location");
                    if (currentLocation != null) {
                        float[] results = new float[1];
                        Location
                            .distanceBetween(currentLocation.latitude, currentLocation.longitude,
                                geoLocation.getLatitude(), geoLocation.getLongitude(), results);
                        tvPlaceDistance.setText("" + (int) results[0] + " м.");
                    } else {
                        restLocation = new LatLng(geoLocation.getLatitude(), geoLocation.getLongitude());
                    }

                    String shortDesc = object.getString("shortdesc");
                    if (shortDesc != null && !shortDesc.isEmpty()) {
                        tvDescription.setText(shortDesc);
                    }

                    final String fullDescription = object.getString("fullDescription");
                    if (fullDescription != null && !fullDescription.isEmpty()) {
                        tvDescription.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvDescription.setText(fullDescription);
                                tvDescriptionMore.setVisibility(View.GONE);
                            }
                        });
                        tvDescriptionMore.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvDescription.setText(fullDescription);
                                tvDescriptionMore.setVisibility(View.GONE);
                            }
                        });
                    }

                    String metro = object.getString("metroDistance");
                    if (metro != null && !metro.isEmpty()) {
                        tvMetro.setText(metro);
                        ivMetro.setVisibility(View.VISIBLE);
                    }

                    JSONArray cuisines = object.getJSONArray("cuisines");
                    if (cuisines != null && cuisines.length() != 0) {
                        List<String> tags = new ArrayList<>();
                        for (int i = 0; i < cuisines.length(); i++) {
                            try {
                                tags.add(cuisines.getString(i));
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                        loadCuisines(tags);
                    }

                    /*Number favoriteCountNumber = object.getNumber("favoriteCount");
                    if (favoriteCountNumber != null) {
                        tvLikesCount.setText(favoriteCountNumber.toString());
                    }*/

                    String address = object.getString("address");
                    if (address != null && !address.isEmpty()) {
                        tvPlaceAddress.setText(address);
                    }

                    String title = object.getString("title");
                    if (title != null && !title.isEmpty()) {
                        tvPlaceTitle.setText(title);
                        findOnMapIntent.putExtra("Title", title);
                    }

                    String workTime = object.getString("workTime");
                    if (workTime != null && !workTime.isEmpty()) {
                        tvWorkTime.setText(workTime);
                    }

                    if (ParseUser.getCurrentUser() != null) {
                        ParseQuery<ParseObject> likeQuery = ParseQuery.getQuery("Favorite");
                        likeQuery.whereEqualTo("place", object);
                        likeQuery.whereEqualTo("user", ParseUser.getCurrentUser());
                        try {
                            isLiked = likeQuery.count() != 0;
                            wasLiked = isLiked;
                            changeFabState();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    }

                    final ParseFile fileMenu = object.getParseFile("mainMenu");
                    if (fileMenu != null) {
                        llMainMenu.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent activityIntent = new Intent(RestaurantActivity.this, MenuActivity.class);
                                activityIntent.putExtra("FileUrl", fileMenu.getUrl());
                                startActivity(activityIntent);
                            }
                        });
                    }

                    final ParseFile drinksMenu = object.getParseFile("drinksMenu");
                    if (drinksMenu != null) {
                        llDrinksMenu.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent activityIntent = new Intent(RestaurantActivity.this, MenuActivity.class);
                                activityIntent.putExtra("FileUrl", drinksMenu.getUrl());
                                startActivity(activityIntent);
                            }
                        });
                    }
                    /*slider.addSlider(
                        new DefaultSliderView(RestaurantActivity.this)
                            .image(persistImage(MainActivity.firstPhoto))
                            .setScaleType(ScaleType.CenterCrop));*/

                    ParseGeoPoint point = object.getParseGeoPoint("location");
                    findOnMapIntent.putExtra("Lat", point.getLatitude());
                    findOnMapIntent.putExtra("Lon", point.getLongitude());

                    loadRestImages(object);
                    loadReviews(object);

                    srlUpdate.setRefreshing(false);
                    srlUpdate.setEnabled(false);
                }
            }
        });
    }

    /*private File persistImage(Bitmap bitmap) {
        File filesDir = getApplicationContext().getCacheDir();
        File imageFile = new File(filesDir, "temp.jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            return null;
        }

        return imageFile;
    }*/

    private Hashtable.Entry<Boolean, String> getWorkingTime(ParseObject scheduleObject) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE", new Locale("en", "US"));
        String dayOfWeekStr = dateFormat.format(date).toLowerCase();

        try {
            scheduleObject.fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String workingHours = scheduleObject.getString(dayOfWeekStr);
        String[] hours = workingHours.split("-");

        int openHours = Integer.parseInt(hours[0]);
        int closeHours = Integer.parseInt(hours[1]);

        int currentHours = calendar.get(Calendar.HOUR_OF_DAY);
        int currentMinutes = calendar.get(Calendar.MINUTE);

        int currentMinutesOfDay = currentHours * 60 + currentMinutes;
        boolean isOpen = currentMinutesOfDay > openHours && currentMinutesOfDay < closeHours;

        String tillTime = "";

        if (isOpen) {
            int tillHours = closeHours / 60;
            if (tillHours > 23) {
                tillHours -= 24;
            }
            if (tillHours == 0) {
                tillTime = "открыто до 00:";
            } else {
                tillTime = "открыто до " + tillHours +":";
            }
            int tillMinutes = closeHours % 60;
            if (tillMinutes == 0) {
                tillTime += "00";
            } else {
                tillTime += tillMinutes;
            }
        } else {
            int tillHours = openHours / 60;
            if (tillHours > 23) {
                tillHours -= 24;
            }
            if (tillHours == 0) {
                tillTime = "открыто до 00:";
            } else {
                tillTime = "открыто до " + tillHours +":";
            }
            int tillMinutes = openHours % 60;
            if (tillMinutes == 0) {
                tillTime += "00";
            } else {
                tillTime += tillMinutes;
            }
        }

        Hashtable.Entry<Boolean, String> result = new SimpleEntry<Boolean, String>(isOpen, tillTime);
        return result;
    }

    private void loadCuisines(List<String> tags) {
        CuisineAdapter adapter = new CuisineAdapter(Cuisine.GET_CUISINES_BY_TAGS(tags));
        rvTypes.setAdapter(adapter);
    }

    private void loadRestImages(ParseObject place) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Gallery");
        query.whereEqualTo("place", place);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    //tvInteriorPhotoCount.setText(objects.size() + " фото");
                    //List<String> pics = new ArrayList<String>();
                    for (ParseObject interiorObject : objects) {
                        ParseFile file = interiorObject.getParseFile("image");
                        /*file.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {
                                if (e == null)
                                    bmps.add(BitmapFactory.decodeByteArray(data, 0, data.length));
                            }
                        });*/
                        bmps.add(file.getUrl());
                        slider.addSlider(
                            new DefaultSliderView(RestaurantActivity.this)
                                .image(file.getUrl())
                                .setScaleType(ScaleType.CenterCrop)
                                .setOnSliderClickListener(
                                    new OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            Intent intent = new Intent(RestaurantActivity.this, SliderActivity.class);
                                            startActivity(intent);
                                        }
                                    }));
                    }
                }
            }
        });
    }

    private void loadReviews(ParseObject place) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Feedback");
        query.whereEqualTo("place", place);
        query.include("user");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    List<Review> reviews = new ArrayList<Review>();
                    for (ParseObject reviewObject : objects) {
                        String username = reviewObject.getParseObject("user").getString("name"),
                            text = reviewObject.getString("text");
                        boolean isPositive = reviewObject.getBoolean("isPositive");
                        Date date = reviewObject.getCreatedAt();
                        reviews.add( new Review(username, date, text, isPositive));
                    }
                    reviewsAdapter = new ReviewsAdapter(reviews);
                    rvReviews.setAdapter(reviewsAdapter);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isLiked != wasLiked) {
                    setResult(7);
                }
                finish();
                return true;
        }

        return false;
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        }else{
            height = 0;
        }

        return height;
    }

    private String getPhoneNumber() {
        SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sPrefs.getString("PhoneNumber", null);
    }

    @Override
    public void onBackPressed() {
        if (isLiked != wasLiked) {
            setResult(7);
        }
        super.onBackPressed();
    }
}
