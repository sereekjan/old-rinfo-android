package kz.ikar.rinfo.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

public class LoadActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStatusBarTranslucent(true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isWatched()) {
                    Intent intent = new Intent(LoadActivity.this, MainActivity.class);
                    LoadActivity.this.startActivity(intent);
                    LoadActivity.this.finish();
                } else {
                    Intent intent = new Intent(LoadActivity.this, SplashActivity.class);
                    LoadActivity.this.startActivity(intent);
                    LoadActivity.this.finish();
                }
            }
        }, 50);
    }

    private boolean isWatched() {
        SharedPreferences sPrefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        return sPrefs.getBoolean("IsWatched", false);
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (makeTranslucent) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            } else {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
    }
}
