package kz.ikar.rinfo.welcomescreen;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import kz.ikar.rinfo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeFragment extends Fragment {
    View.OnClickListener skipListener, nextListener;
    String description;
    int drawable;

    Button btnSkip, btnNext;
    ImageView ivImage;
    TextView tvDescription;

    public WelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slider, container, false);

        ivImage = view.findViewById(R.id.iv_img);
        tvDescription = view.findViewById(R.id.tv_description);
        btnSkip = view.findViewById(R.id.btn_skip);
        btnNext = view.findViewById(R.id.btn_next);

        ivImage.setImageResource(drawable);
        tvDescription.setText(description);
        btnSkip.setOnClickListener(skipListener);
        btnNext.setOnClickListener(nextListener);

        return view;
    }

    public void setBtnSkipListener(View.OnClickListener listener) {
        skipListener = listener;
    }

    public void setBtnNextListener(View.OnClickListener listener) {
        nextListener = listener;
    }

    public void setImageDrawable(int drawable) {
        this.drawable = drawable;
    }

    public void setDescription(String text) {
        description = text;
    }
}
