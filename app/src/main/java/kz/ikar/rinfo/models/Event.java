package kz.ikar.rinfo.models;

/**
 * Created by User on 29.01.2018.
 */

public class Event {
    private String id;
    private String title;
    private String time;
    private String description;
    private String subDescription;
    private String logoUrl;

    public Event(String id, String title, String time, String description,
        String subDescription, String logoUrl) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.description = description;
        this.subDescription = subDescription;
        this.logoUrl = logoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubDescription() {
        return subDescription;
    }

    public void setSubDescription(String subDescription) {
        this.subDescription = subDescription;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
}
