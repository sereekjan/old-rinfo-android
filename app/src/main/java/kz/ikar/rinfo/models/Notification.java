package kz.ikar.rinfo.models;

/**
 * Created by User on 05.03.2018.
 */

public class Notification {
    String id;
    String text;
    String time;
    String imgUrl;

    public Notification(String id, String text, String time, String imgUrl) {
        this.id = id;
        this.text = text;
        this.time = time;
        this.imgUrl = imgUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
