package kz.ikar.rinfo.models;

/**
 * Created by User on 29.01.2018.
 */

public class Discount {
    private String id;
    private String title;
    private String time;
    private String description;
    private String logoUrl;
    private String placeTitle;
    private int placesCount;
    private String city;

    public Discount(String id, String title, String time, String description, String logoUrl,
        String placeTitle, int placesCount, String city) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.description = description;
        this.logoUrl = logoUrl;
        this.placeTitle = placeTitle;
        this.placesCount = placesCount;
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public int getPlacesCount() {
        return placesCount;
    }

    public void setPlacesCount(int placesCount) {
        this.placesCount = placesCount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPlaceTitle() {
        return placeTitle;
    }

    public void setPlaceTitle(String placeTitle) {
        this.placeTitle = placeTitle;
    }
}
