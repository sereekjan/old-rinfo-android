package kz.ikar.rinfo.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by User on 12.02.2018.
 */

public class RestaurantPoint {
    private String id;
    private String title;
    private LatLng position;
    private String address;

    public RestaurantPoint(String id, String title, LatLng position, String address) {
        this.id = id;
        this.title = title;
        this.position = position;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
