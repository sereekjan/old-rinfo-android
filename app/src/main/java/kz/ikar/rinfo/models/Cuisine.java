package kz.ikar.rinfo.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kz.ikar.rinfo.R;

/**
 * Created by User on 16.02.2018.
 */

public class Cuisine {
    private String title;
    private String tag;
    private int mipmapId;
    private boolean isChecked;
    private long price;

    public Cuisine(String title, String tag, int mipmapId, boolean isChecked) {
        this.title = title;
        this.tag = tag;
        this.mipmapId = mipmapId;
        this.isChecked = isChecked;
    }

    public Cuisine(long price) {
        this.price = price;
    }

    public static String GET_SOME_FEATURES_TITLES() {
        List<Cuisine> cuisines = GET_SELECTED_FEATURES();
        if (cuisines.size() == 0) {
            return "Не выбрано";
        }

        if (cuisines.size() == 1) {
            return cuisines.get(0).getTitle();
        }

        if (cuisines.size() >= 2) {
            String titles = "";
            titles += cuisines.get(0).getTitle();
            titles += ", ";
            titles += cuisines.get(1).getTitle();

            if (cuisines.size() == 2)
                return titles;

            titles += "..";
            return titles;
        }

        return "Не выбрано";
    }

    public static String GET_SOME_NATIONS_TITLES() {
        List<Cuisine> cuisines = GET_SELECTED_NATIONS();
        if (cuisines.size() == 0) {
            return "Не выбрано";
        }

        if (cuisines.size() == 1) {
            return cuisines.get(0).getTitle();
        }

        if (cuisines.size() >= 2) {
            String titles = "";
            titles += cuisines.get(0).getTitle();
            titles += ", ";
            titles += cuisines.get(1).getTitle();

            if (cuisines.size() == 2)
                return titles;

            titles += "..";
            return titles;
        }

        return "Не выбрано";
    }

    public static List<Cuisine> GET_CUISINES_BY_TAGS(List<String> tags) {
        List<Cuisine> cuisines = new ArrayList<>();

        for (Cuisine cuisine: LIST_FEATURES) {
            if (tags.contains(cuisine.getTag())) {
                cuisines.add(cuisine);
            }
        }

        for (Cuisine cuisine: LIST_NATIONS) {
            if (tags.contains(cuisine.getTag())) {
                cuisines.add(cuisine);
            }
        }

        return cuisines;
    }

    public static List<String> GET_TAGS() {
        List<String> tags = new ArrayList<>();

        for (Cuisine cuisine: GET_SELECTED()) {
            tags.add(cuisine.getTag());
        }

        return tags;
    }

    public static List<Cuisine> GET_SELECTED() {
        List<Cuisine> cuisines = GET_SELECTED_FEATURES();
        cuisines.addAll(GET_SELECTED_NATIONS());
        return cuisines;
    }

    private static List<Cuisine> GET_SELECTED_FEATURES() {
        List<Cuisine> cuisines = new ArrayList<>();

        for (Cuisine cuisine: LIST_FEATURES) {
            if (cuisine.isChecked) {
                cuisines.add(cuisine);
            }
        }

        return cuisines;
    }

    private static List<Cuisine> GET_SELECTED_NATIONS() {
        List<Cuisine> cuisines = new ArrayList<>();

        for (Cuisine cuisine: LIST_NATIONS) {
            if (cuisine.isChecked) {
                cuisines.add(cuisine);
            }
        }

        return cuisines;
    }

    public static List<Cuisine> LIST_FEATURES = Arrays.asList(
        new Cuisine("Бургеры", "burger", R.mipmap.burger, false),
        new Cuisine("Крафтовое пиво", "craftedBeer", R.mipmap.beer, false),
        new Cuisine("Видеоигры", "videoGames", R.mipmap.videogames, false),
        new Cuisine("Вегетарианцы", "vegeterian", R.mipmap.vegetarian, false),
        new Cuisine("Живая музыка", "liveMusic", R.mipmap.live_music, false),
        new Cuisine("ВайФай", "wifi", R.mipmap.wifi, false),
        new Cuisine("Завтраки, бизнес-ланчи", "breakfast", R.mipmap.breakfast, false),
        new Cuisine("Настольные игры", "tabletopGames", R.mipmap.tabletop_games, false),
        new Cuisine("Веранда", "veranda", R.mipmap.veranda, false),
        new Cuisine("Летник", "outdoorCafe", R.mipmap.outdoor_cafe, false),
        new Cuisine("Шоу программа", "show", R.mipmap.show, false),
        new Cuisine("Аниматоры для детей", "animators", R.mipmap.animators, false),
        new Cuisine("Есть парковка", "parking", R.mipmap.parking, false),
        new Cuisine("Оплата картой", "cardPayment", R.mipmap.card_payment, false),
        new Cuisine("Авторская кухня", "uniqueMeal", R.mipmap.unique_meal, false),
        new Cuisine("Винная карта", "wineMap", R.mipmap.wine_map, false),
        new Cuisine("Кальян", "hookah", R.mipmap.hookah, false),
        new Cuisine("Диджей", "dj", R.mipmap.dj, false),
        new Cuisine("Караоке", "karaoke", R.mipmap.karaoke, false),
        new Cuisine("Панорамный вид", "panoramicView", R.mipmap.panoramic_view, false),
        new Cuisine("Скидки именинникам", "birthdayDiscounts", R.mipmap.birthday_discounts, false),
        new Cuisine("Фестивали", "festivals", R.mipmap.festivals, false),
        new Cuisine("Паб", "pub", R.mipmap.pub, false),
        new Cuisine("Спортивные трансляции", "sportBroadcast", R.mipmap.sport_broadcast, false),
        new Cuisine("Десткая комната", "kidRoom", R.mipmap.kid_room, false),
        new Cuisine("Коктейли", "coctails", R.mipmap.cocktails, false),
        new Cuisine("Вечеринки, танцы", "danceparty", R.mipmap.danceparty, false),
        new Cuisine("Десерты", "desserts", R.mipmap.deserts, false),
        new Cuisine("Фейсконтроль и дресскод", "faceControl", R.mipmap.face_control, false),
        new Cuisine("Тематические вечеринки", "thematicParties", R.mipmap.thematic_parties, false),
        new Cuisine("Ночной клуб", "nightClub", R.mipmap.night_club, false),
        new Cuisine("Уникальный интерьер", "uniqueInterior", R.mipmap.unique_interior, false),
        new Cuisine("Кафе", "cafe", R.mipmap.cafe, false),
        new Cuisine("Курилка", "smoking", R.mipmap.smoking, false),
        new Cuisine("На природе", "nature", R.mipmap.nature, false),
        new Cuisine("У воды", "coast", R.mipmap.coast, false),
        new Cuisine("Рыбные блюда", "fish", R.mipmap.fish, false),
        new Cuisine("Мясные блюда", "meat", R.mipmap.meat, false),
        new Cuisine("Шашлыки", "kebab", R.mipmap.kebab, false),
        new Cuisine("ВИП кабинки", "vip", R.mipmap.vip, false),
        new Cuisine("Диетическое меню", "diet", R.mipmap.diet, false),
        new Cuisine("Кофейня", "coffee", R.mipmap.coffee, false),
        new Cuisine("Доставка еды", "delivery", R.mipmap.delivery, false),
        new Cuisine("Стейки", "steak", R.mipmap.steak, false),
        new Cuisine("Новое заведение", "newPlace", R.mipmap.new_place, false),
        new Cuisine("Деловые приемы", "official", R.mipmap.official, false),
        new Cuisine("Правый берег", "rightCoast", R.mipmap.side_cost, false),
        new Cuisine("Левый берег", "leftCoast", R.mipmap.side_cost, false),
        new Cuisine("Круглосуточно", "open", R.mipmap.open, false),
        new Cuisine("Корпоративы", "corporate", R.mipmap.corporate, false),
        new Cuisine("Банкеты", "banquet", R.mipmap.banquet, false));

    public static List<Cuisine> LIST_NATIONS = Arrays.asList(
        new Cuisine("Китайская кухня", "chineese", R.mipmap.chinese, false),
        new Cuisine("Итальянская кухня", "italian", R.mipmap.italian, false),
        new Cuisine("Европейская кухня", "european", R.mipmap.european, false),
        new Cuisine("Морепродукты", "seafoods", R.mipmap.seafood, false),
        new Cuisine("Мескиканская кухня", "mexican", R.mipmap.mexican, false),
        new Cuisine("Американская кухня", "american", R.mipmap.american, false),
        new Cuisine("Японская кухня", "japaneese", R.mipmap.japanese, false),
        new Cuisine("Турецкая кухня", "turkish", R.mipmap.turkish, false),
        new Cuisine("Грузинская кухня", "georgian", R.mipmap.georgian, false),
        new Cuisine("Азиатская кухня", "asian", R.mipmap.asian, false),
        new Cuisine("Восточная кухня", "east", R.mipmap.eastern, false),
        new Cuisine("Корейская кухня", "korean", R.mipmap.korean, false),
        new Cuisine("Индийская кухня", "indian", R.mipmap.indian, false));

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getMipmapId() {
        return mipmapId;
    }

    public void setMipmapId(int mipmapId) {
        this.mipmapId = mipmapId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
