package kz.ikar.rinfo.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 08.01.2018.
 */

public class Category {
    private String id;
    private String name;
    private int count;
    private String logoUrl;

    public Category(String id, String name, int count, String logoUrl) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.logoUrl = logoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    /*public static List<Category> getTempData() {
        List<Category> categories = new ArrayList<>();

        categories.add(new Category("Рядом с вами", 15, "http://www.designyourway.net/diverse/restintdes/Toast-2.jpg"));
        categories.add(new Category("20-ка лучших", 20, "https://theunbearablelightnessofbeinghungry.com/wp-content/uploads/2016/10/Monopole-Interior-Wide-4-copy-590x332.jpg"));
        categories.add(new Category("Уникальный интерьер", 43, "http://www.designyourway.net/diverse/restintdes/Toast-2.jpg"));
        categories.add(new Category("AB Restaurants", 8, "https://theunbearablelightnessofbeinghungry.com/wp-content/uploads/2016/10/Monopole-Interior-Wide-4-copy-590x332.jpg"));
        categories.add(new Category("Рядом с вами", 15, "http://www.designyourway.net/diverse/restintdes/Toast-2.jpg"));
        categories.add(new Category("20-ка лучших", 20, "https://theunbearablelightnessofbeinghungry.com/wp-content/uploads/2016/10/Monopole-Interior-Wide-4-copy-590x332.jpg"));
        categories.add(new Category("Уникальный интерьер", 43, "http://www.designyourway.net/diverse/restintdes/Toast-2.jpg"));
        categories.add(new Category("AB Restaurants", 8, "https://theunbearablelightnessofbeinghungry.com/wp-content/uploads/2016/10/Monopole-Interior-Wide-4-copy-590x332.jpg"));

        return categories;
    }*/
}
