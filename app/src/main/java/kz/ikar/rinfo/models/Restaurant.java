package kz.ikar.rinfo.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 08.01.2018.
 */

public class Restaurant implements Serializable {
    private String id;
    private String location;
    private String avgCost;
    private String name;
    private String types;
    private String description;
    private int likes;
    private String logoUrl;
    private String discountValue;
    private String discountTime;
    private boolean isFavorite;

    public Restaurant(String id, String location, String avgCost, String name, String types, String description, int likes, String logoUrl, String discountValue, String discountTime) {
        this.id = id;
        this.location = location;
        this.avgCost = avgCost;
        this.name = name;
        this.types = types;
        this.description = description;
        this.likes = likes;
        this.logoUrl = logoUrl;
        this.discountValue = discountValue;
        this.discountTime = discountTime;
    }

    public void setDiscount(String discountValue, String discountTime) {
        this.discountValue = discountValue;
        this.discountTime = discountTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(String avgCost) {
        this.avgCost = avgCost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public String getDiscountTime() {
        return discountTime;
    }

    public void setDiscountTime(String discountTime) {
        this.discountTime = discountTime;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    /*public static List<Restaurant> getTempData() {
        List<Restaurant> restaurants = new ArrayList<>();

        restaurants.add(new Restaurant("",
                "Рядом (1 560м.)",
                "3 500 ₸",
                "У Афанасича",
                "Русская кухня, суши, роллы, японская кухня",
                "Здесь истинных гурманов ждут потрясающе вкусные блюда русской, кавказской, итальянской и японской куъонь. Разнообразие и обилие предлагаемых кулинарных шедевров удивит самого критичного любителя вкусно поесть.",
                98,
                "https://u.tfstatic.com/restaurant_photos/389/258389/169/612/restaurant-c-restaurantzaal-6aaf5.jpg"));

        restaurants.add(new Restaurant("",
                "Рядом (1 560м.)",
                "5 500 ₸",
                "Стейкхаус Crudo",
                "Стейкхаус, американская кухня",
                "",
                112,
                "https://s.inyourpocket.com/gallery/24276.jpg"));

        restaurants.add(new Restaurant("",
                "Рядом (1 560м.)",
                "3 500 ₸",
                "SMUG Burger",
                "Бургеры, американская кухня",
                "",
                200,
                "https://media-cdn.tripadvisor.com/media/photo-o/06/cd/3d/f2/ludwig-das-burger-restaurant.jpg"));

        Restaurant restaurant = new Restaurant("",
                "Рядом (1 560м.)",
                "3 500 ₸",
                "Fellini",
                "Итальянская кухня, паста, пицца",
                "",
                2,
                "https://media-cdn.tripadvisor.com/media/photo-s/0a/55/ed/2d/one-of-dubai-s-best-italian.jpg");
        restaurant.setDiscount(30, "13:00 - 15:00");
        restaurants.add(restaurant);

        return restaurants;
    }*/
}
