package kz.ikar.rinfo.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by User on 08.02.2018.
 */

public class Review {
    private String username;
    private String date;
    private String text;
    private boolean isPositive;

    public Review(String username, Date date, String text, boolean isPositive) {
        this.username = username;
        this.date = new SimpleDateFormat("dd MMM yyyy 'в' HH:mm", new Locale("ru", "RU")).format(date);
        this.text = text;
        this.isPositive = isPositive;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isPositive() {
        return isPositive;
    }

    public void setPositive(boolean positive) {
        isPositive = positive;
    }
}
