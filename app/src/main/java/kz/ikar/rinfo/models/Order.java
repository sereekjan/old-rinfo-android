package kz.ikar.rinfo.models;

/**
 * Created by User on 07.03.2018.
 */

public class Order {
    private String id;
    private String placeTitle;
    private String placeImgUrl;
    private String details;
    private String status;

    public Order(String id, String placeTitle, String placeImgUrl, String details,
        String status) {
        this.id = id;
        this.placeTitle = placeTitle;
        this.placeImgUrl = placeImgUrl;
        this.details = details;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlaceTitle() {
        return placeTitle;
    }

    public void setPlaceTitle(String placeTitle) {
        this.placeTitle = placeTitle;
    }

    public String getPlaceImgUrl() {
        return placeImgUrl;
    }

    public void setPlaceImgUrl(String placeImgUrl) {
        this.placeImgUrl = placeImgUrl;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
