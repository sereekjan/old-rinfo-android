package kz.ikar.rinfo;

import android.app.Application;
import com.parse.Parse;

/**
 * Created by User on 22.01.2018.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Parse.initialize(new Parse.Configuration.Builder(this)
            .applicationId("rinfo-appid")
            .clientKey("rinfo-clientkey")
            .server("http://ec2-35-158-227-89.eu-central-1.compute.amazonaws.com:1337/api/")
            .build());
    }
}
