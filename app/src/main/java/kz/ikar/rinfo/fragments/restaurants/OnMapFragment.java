package kz.ikar.rinfo.fragments.restaurants;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.RestaurantActivity;
import kz.ikar.rinfo.models.Restaurant;
import kz.ikar.rinfo.models.RestaurantPoint;

public class OnMapFragment extends Fragment {

    MapView mapView;
    private GoogleMap googleMap;
    private List<RestaurantPoint> points;

    public OnMapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_on_map, container, false);

        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        mapView.onResume();

        MapsInitializer.initialize(getActivity().getApplicationContext());

        if (isConnectedToInternet(getActivity())) {
            loadMap();
        }

        return view;
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private void loadMap() {
        points = new ArrayList<>();

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap gMap) {
                googleMap = gMap;


                googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {
                        View rootView = getActivity().getLayoutInflater().inflate(R.layout.fragment_info_window, null);

                        LatLng latLng = marker.getPosition();

                        TextView tvTitle = rootView.findViewById(R.id.tv_title);
                        TextView tvAddress = rootView.findViewById(R.id.tv_address);

                        tvTitle.setText(marker.getTitle());
                        tvAddress.setText(marker.getSnippet());

                        return rootView;
                    }
                });

                googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent activityIntent = new Intent(getActivity(), RestaurantActivity.class);
                        activityIntent.putExtra("RestaurantId", (String) marker.getTag());
                        startActivity(activityIntent);
                    }
                });

                //googleMap.setMyLocationEnabled(true);
                LatLng astana = new LatLng(51.137898, 71.439965);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(astana).zoom(12f).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                loadRestaurantPoints();
                if (googleMap != null) {
                    for (final RestaurantPoint point: points) {
                        final MarkerOptions options = new MarkerOptions();
                        options.title(point.getTitle());
                        options.position(point.getPosition());
                        options.snippet(point.getAddress());
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Marker marker = googleMap.addMarker(options);
                                marker.setTag(point.getId());
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void loadRestaurantPoints() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Place");
        List<ParseObject> places = null;
        try {
            places = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (places != null) {
            for (ParseObject object: places) {
                ParseGeoPoint parseGeoPoint = object.getParseGeoPoint("location");
                if (parseGeoPoint != null) {
                    LatLng latLng = new LatLng(
                        parseGeoPoint.getLatitude(),
                        parseGeoPoint.getLongitude());

                    points.add(new RestaurantPoint(
                        object.getObjectId(),
                        object.getString("title"),
                        latLng,
                        object.getString("address")));
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mapView != null)
            mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mapView != null)
            mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }
}
