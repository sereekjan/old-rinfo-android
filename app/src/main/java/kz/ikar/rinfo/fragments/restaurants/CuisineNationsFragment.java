package kz.ikar.rinfo.fragments.restaurants;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.LinearLayout;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.MainActivity;
import kz.ikar.rinfo.adapters.FilterCuisineAdapter;
import kz.ikar.rinfo.adapters.FilterCuisineAdapter.OnItemEventListener;
import kz.ikar.rinfo.models.Cuisine;

/**
 * A simple {@link Fragment} subclass.
 */
public class CuisineNationsFragment extends Fragment {
    Button btnClear;
    RecyclerView rvCuisines;

    public CuisineNationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_cuisine_choose, container, false);

        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        btnClear = rootView.findViewById(R.id.btn_clear);
        rvCuisines = rootView.findViewById(R.id.rv_cuisines);

        rvCuisines.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Cuisine cuisine: Cuisine.LIST_NATIONS) {
                    cuisine.setChecked(false);
                }

                loadCuisines();
            }
        });

        loadCuisines();
    }

    private void loadCuisines() {
        FilterCuisineAdapter adapter = new FilterCuisineAdapter(Cuisine.LIST_NATIONS,
            new OnItemEventListener() {
                @Override
                public void onItemClicked(String tag, boolean isChecked) {

                }
            });
        rvCuisines.setAdapter(adapter);
    }
}
