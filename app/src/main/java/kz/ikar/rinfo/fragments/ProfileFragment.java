package kz.ikar.rinfo.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitActivity.ResponseType;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.SkinManager.Skin;
import com.facebook.accountkit.ui.UIManager;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.fragments.profile.FavoritePlacesFragment;
import kz.ikar.rinfo.fragments.profile.NotificationsFragment;
import kz.ikar.rinfo.fragments.profile.OrderHistoryFragment;
import kz.ikar.rinfo.fragments.profile.ProfileChangeFragment;
import kz.ikar.rinfo.fragments.profile.SettingsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    LinearLayout llMain;
    Button btnAuth;
    Button btnSettings;
    TextView tvName;
    TextView tvPhone;
    TextView tvEmail;
    TextView tvFavoriteCount;
    TextView tvHistoryCount;
    CircleImageView civProfileImage;
    LinearLayout llEditProfile;
    LinearLayout llFavoritePlaces;
    LinearLayout llOrderHistory;
    LinearLayout llNotifications;
    LinearLayout llWrite;
    TextView tvNoConnection;
    SwipeRefreshLayout srlUpdate;

    FrameLayout flMain;

    public static final int REQUEST_CODE = 999;
    Handler mainHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        llMain = view.findViewById(R.id.ll_main);
        btnAuth = view.findViewById(R.id.btn_auth);
        btnSettings = view.findViewById(R.id.btn_settings);
        tvName = view.findViewById(R.id.tv_name);
        tvPhone = view.findViewById(R.id.tv_phone);
        tvEmail = view.findViewById(R.id.tv_email);
        tvHistoryCount = view.findViewById(R.id.tv_history_count);
        tvFavoriteCount = view.findViewById(R.id.tv_favorite_count);
        civProfileImage = view.findViewById(R.id.civ_profile_image);
        llEditProfile = view.findViewById(R.id.ll_edit_profile);
        llFavoritePlaces = view.findViewById(R.id.ll_favorite_places);
        llOrderHistory = view.findViewById(R.id.ll_order_history);
        llNotifications = view.findViewById(R.id.ll_notifications);
        llWrite = view.findViewById(R.id.ll_write);
        tvNoConnection = view.findViewById(R.id.tv_no_connection);
        srlUpdate = view.findViewById(R.id.srl_update);
        flMain = view.findViewById(R.id.content_main);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llEditProfile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_main, new ProfileChangeFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        llOrderHistory.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_main, new OrderHistoryFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        llFavoritePlaces.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_main, new FavoritePlacesFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        llNotifications.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_main, new NotificationsFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        llWrite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(Intent.ACTION_SENDTO);
                String uriText = "mailto:" + Uri.encode("rinfo.adm@gmail.com");
                Uri uri = Uri.parse(uriText);
                send.setData(uri);
                startActivity(Intent.createChooser(send, "Send mail..."));
            }
        });

        btnSettings.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_main, new SettingsFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        mainHandler = new Handler();

        srlUpdate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet(getActivity())) {
                    loadData();
                    tvNoConnection.setVisibility(View.GONE);
                    flMain.setVisibility(View.VISIBLE);
                } else {
                    flMain.setVisibility(View.GONE);
                    tvNoConnection.setVisibility(View.VISIBLE);
                    srlUpdate.setRefreshing(false);
                }
            }
        });

        if (isConnectedToInternet(getActivity())) {
            loadData();
        } else {
            flMain.setVisibility(View.GONE);
            tvNoConnection.setVisibility(View.VISIBLE);
        }
    }

    private void loadData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isAuthorized()) {
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            llMain.setVisibility(View.VISIBLE);
                            btnAuth.setVisibility(View.GONE);
                            srlUpdate.setRefreshing(false);
                        }
                    });

                    try {
                        loadUser();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            llMain.setVisibility(View.GONE);
                            btnAuth.setVisibility(View.VISIBLE);

                            btnAuth.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UIManager uiManager = new SkinManager(Skin.CONTEMPORARY,
                                        ContextCompat.getColor(getActivity(), R.color.colorAccent));
                                    Intent authIntent = new Intent(getActivity(), AccountKitActivity.class);
                                    AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                                        new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                                            ResponseType.TOKEN);
                                    configurationBuilder.setUIManager(uiManager);
                                    authIntent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build());
                                    startActivityForResult(authIntent, REQUEST_CODE);
                                    srlUpdate.setRefreshing(false);
                                }
                            });
                        }
                    });
                }
            }
        }).start();
    }

    public static boolean isConnectedToInternet(Context context) {

        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    public void loadUser() throws ParseException {
        ParseUser.logIn(ParseUser.getCurrentUser().getUsername(), "zkenes" + ParseUser.getCurrentUser().getUsername());
        ParseUser object = ParseUser.getCurrentUser();

        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        if (parseInstallation.getParseObject("user") == null) {
            parseInstallation.put("user", object);
            parseInstallation.saveInBackground();
        }

        final String name = object.getString("name");
        final String email = object.getString("email");

        final ParseFile profileImage = object.getParseFile("profileImage");

        ParseQuery<ParseObject> historyQuery = ParseQuery.getQuery("Reservation");
        ParseQuery<ParseObject> favoriteQuery = ParseQuery.getQuery("Favorite");

        historyQuery.whereEqualTo("user", object);
        favoriteQuery.whereEqualTo("user", object);

        int favCount = 0, histCount = 0;
        try {
            favCount = favoriteQuery.count();
            histCount = historyQuery.count();
        } catch (ParseException e1) {
            e1.printStackTrace();
        }

        final int finalFavCount = favCount;
        final int finalHistCount = histCount;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (name != null) {
                    tvName.setText(name);
                } else {
                    tvName.setText("Имя пользователя");
                }

                if (email != null) {
                    tvEmail.setText(email);
                } else {
                    tvEmail.setText("Почта");
                }

                tvFavoriteCount.setText("" + finalFavCount);
                tvHistoryCount.setText("" + finalHistCount);

                tvPhone.setText(ParseUser.getCurrentUser().getUsername());

                if (profileImage != null) {
                    Picasso.with(civProfileImage.getContext())
                            .load(profileImage.getUrl())
                            .fit().centerCrop()
                            .into(civProfileImage);
                }
            }
        });
    }

    private boolean isUserRegistered(String phoneNumber) {
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("User");
        parseQuery.whereEqualTo("username", phoneNumber);
        try {
            if (parseQuery.count() != 0) {
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void registrate(String phoneNumber) {
        ParseUser user = new ParseUser();
        user.setUsername(phoneNumber);
        user.setPassword("zkenes" + phoneNumber);
        user.put("city", "Астана");
        try {
            user.signUp();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            AccountKitLoginResult result = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if (result.getError() != null) {
                Toast.makeText(getActivity(), "" + result.getError().getErrorType().getMessage(), Toast.LENGTH_SHORT).show();
                return;
            } else if (result.wasCancelled()) {
                Toast.makeText(getActivity(), "Отмена", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Вы успешно авторизовались!", Toast.LENGTH_SHORT).show();
                //saveToken(result.getAccessToken().getToken());
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        PhoneNumber number = account.getPhoneNumber();
                        String currentUserPhoneNumber = "+";
                        currentUserPhoneNumber += number.getCountryCode();
                        currentUserPhoneNumber += number.getPhoneNumber();

                        if (!isUserRegistered(currentUserPhoneNumber)) {
                            registrate(currentUserPhoneNumber);
                        }

                        authorize(currentUserPhoneNumber, "zkenes" + currentUserPhoneNumber);
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {

                    }
                });
            }
        }
    }

    private boolean isAuthorized() {
        //AccessToken accountKit = AccountKit.getCurrentAccessToken();
        ParseUser currentUser = ParseUser.getCurrentUser();

        if (currentUser != null) {
            return true;
        } else {
            return false;
        }
    }

    private void authorize(final String username, final String password) {
        llMain.setVisibility(View.VISIBLE);
        btnAuth.setVisibility(View.GONE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ParseUser.logIn(username, password);
                    loadUser();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /*private void savePhoneNumber(String phoneNumber) {
        if (getActivity() != null) {
            SharedPreferences sPrefs = PreferenceManager
                .getDefaultSharedPreferences(getActivity().getApplicationContext());
            sPrefs.edit().putString("PhoneNumber", phoneNumber).apply();
        }
    }

    private String getPhoneNumber() {
        if (getActivity() != null) {
            SharedPreferences sPrefs = PreferenceManager
                .getDefaultSharedPreferences(getActivity().getApplicationContext());
            return sPrefs.getString("PhoneNumber", null);
        }

        return null;
    }*/
}
