package kz.ikar.rinfo.fragments.discounts;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.util.ArrayList;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.DiscountActivity;
import kz.ikar.rinfo.adapters.DiscountsAdapter;
import kz.ikar.rinfo.adapters.DiscountsAdapter.OnItemEventListener;
import kz.ikar.rinfo.models.Discount;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscountsListFragment extends Fragment {
    RecyclerView rvDiscounts;
    TextView tvNoConnection;
    SwipeRefreshLayout srlUpdate;

    public static final String TAG = "RINFO";

    private DiscountsAdapter adapter;

    public DiscountsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_discounts_list, container, false);

        rvDiscounts = (RecyclerView) view.findViewById(R.id.rv_discounts);
        tvNoConnection = view.findViewById(R.id.tv_no_connection);
        srlUpdate = view.findViewById(R.id.srl_update);

        rvDiscounts.setLayoutManager(new GridLayoutManager(getActivity(), 1, LinearLayoutManager.VERTICAL, false));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        srlUpdate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet(getActivity())) {
                    adapter = null;
                    loadDiscountsAsync();
                    tvNoConnection.setVisibility(View.GONE);
                    rvDiscounts.setVisibility(View.VISIBLE);
                } else {
                    rvDiscounts.setVisibility(View.GONE);
                    tvNoConnection.setVisibility(View.VISIBLE);
                    srlUpdate.setRefreshing(false);
                }
            }
        });

        if (isConnectedToInternet(getActivity())) {
            adapter = null;
            loadDiscountsAsync();
        } else {
            rvDiscounts.setVisibility(View.GONE);
            tvNoConnection.setVisibility(View.VISIBLE);
        }
    }

    private void loadDiscountsAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Discount> discounts = getDiscounts();
                adapter = new DiscountsAdapter(discounts,
                    new OnItemEventListener() {
                        @Override
                        public void onBottomReached(int position) {
                            UpdateListAsyncTask asyncTask = new UpdateListAsyncTask();
                            asyncTask.execute();
                            //adapter.datasetChanged(discounts);
                        }

                        @Override
                        public void onItemClicked(String id) {
                            Intent activityIntent = new Intent(getActivity(), DiscountActivity.class);
                            activityIntent.putExtra("DiscountId", id);
                            startActivity(activityIntent);
                        }
                    });

                Handler handler = new Handler(getActivity().getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        rvDiscounts.setAdapter(adapter);
                        srlUpdate.setRefreshing(false);
                    }
                });
            }
        }).start();
    }

    public static boolean isConnectedToInternet(Context context) {

        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private List<Discount> getDiscounts() {
        List<Discount> discounts = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("SpecialOffer");
        query.include("place");
        query.setLimit(10);

        int skipCount = 10 * (adapter == null ? 0 : ((adapter.getItemCount() + 9) / 10));
        query.setSkip(skipCount);

        List<ParseObject> objects = null;
        try {
            objects = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (objects != null) {
            for (ParseObject event : objects) {

                Discount discount = new Discount(
                    event.getObjectId(),
                    event.getString("title"),
                    event.getString("dueToTitle"),
                    event.getString("fullDescription"),
                    event.getParseFile("image").getUrl(),
                    event.getParseObject("place").getString("title"),
                    1,
                    event.getString("city")
                );

                discounts.add(discount);
            }
        }

        return discounts;
    }

    private class UpdateListAsyncTask extends AsyncTask<Integer, Void, List<Discount>> {

        @Override
        protected List<Discount> doInBackground(Integer... integers) {
            return getDiscounts();
        }

        @Override
        protected void onPostExecute(List<Discount> discounts) {
            adapter.datasetChanged(discounts);
        }
    }
}
