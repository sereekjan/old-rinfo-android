package kz.ikar.rinfo.fragments.profile;


import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import java.io.File;
import kz.ikar.rinfo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileChangeFragment extends Fragment {
    CircleImageView civProfileImage;
    Button btnChangeProfileImage;
    Button btnReady;
    TextView tvName;
    TextView tvCity;
    TextView tvPhone;
    TextView tvEmail;
    LinearLayout llName;
    LinearLayout llCity;
    LinearLayout llPhone;
    LinearLayout llEmail;
    String changedName, changedEmail;
    ParseFile changedProfileImageFile;

    private final int RESULT_LOAD_IMG = 100;
    public static final int READ_EXTERNAL_STORAGE_PERMISSION = 200;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_change, container, false);

        civProfileImage = view.findViewById(R.id.civ_profile_image);
        btnChangeProfileImage = view.findViewById(R.id.btn_change_profile_image);
        btnReady = view.findViewById(R.id.btn_ready);
        tvName = view.findViewById(R.id.tv_name);
        tvCity = view.findViewById(R.id.tv_city);
        tvPhone = view.findViewById(R.id.tv_phone);
        tvEmail = view.findViewById(R.id.tv_email);
        llName = view.findViewById(R.id.ll_name);
        llCity = view.findViewById(R.id.ll_city);
        llPhone = view.findViewById(R.id.ll_phone);
        llEmail = view.findViewById(R.id.ll_email);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadProfile();

        btnReady.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (changedName == null && changedEmail == null && changedProfileImageFile == null) {
                    getActivity().onBackPressed();
                    return;
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ParseUser currentUser = ParseUser.getCurrentUser();
                        if (changedName != null) {
                            currentUser.put("name", changedName);
                        }

                        if (changedEmail != null) {
                            currentUser.put("email", changedEmail);
                        }

                        if (changedProfileImageFile != null) {
                            try {
                                changedProfileImageFile.save();
                                currentUser.put("profileImage", changedProfileImageFile);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        try {
                            currentUser.save();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                getActivity().onBackPressed();
            }
        });

        llName.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Get the layout inflater
                LayoutInflater inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View rootView = inflater.inflate(R.layout.dialog_profile_change, null);
                final EditText etText = rootView.findViewById(R.id.et_text);
                builder.setView(rootView)
                    .setTitle("Ваше имя")
                    // Add action buttons
                    .setPositiveButton("ОК", null/*, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {


                        }
                    }*/)
                    .setNegativeButton("Отмена", null/*, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }*/);
                final AlertDialog alertDialog = builder.create();
                alertDialog.setOnShowListener(new OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        final Button btnOk = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        Button btnCancel = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

                        btnOk.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (etText.getText() != null && etText.getText().length() > 1) {
                                    changedName = etText.getText().toString();
                                    tvName.setText(changedName);
                                    alertDialog.dismiss();
                                } else {
                                    etText.setText("");
                                }
                            }
                        });

                        btnCancel.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });
                    }
                });
                alertDialog.show();
            }
        });

        llEmail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Get the layout inflater
                LayoutInflater inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View rootView = inflater.inflate(R.layout.dialog_profile_change, null);
                final EditText etText = rootView.findViewById(R.id.et_text);
                etText.setHint("Введите верный E-mail");
                etText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                builder.setView(rootView)
                    .setTitle("Ваша почта")
                    // Add action buttons
                    .setPositiveButton("ОК", null/*, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {


                        }
                    }*/)
                    .setNegativeButton("Отмена", null/*, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }*/);
                final AlertDialog alertDialog = builder.create();
                alertDialog.setOnShowListener(new OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        final Button btnOk = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        Button btnCancel = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

                        btnOk.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (etText.getText() != null && isValidEmail(etText.getText())) {
                                    changedEmail = etText.getText().toString();
                                    tvEmail.setText(changedEmail);
                                    alertDialog.dismiss();
                                } else {
                                    etText.setText("");
                                }
                            }
                        });

                        btnCancel.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });
                    }
                });
                alertDialog.show();
            }
        });

        btnChangeProfileImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_EXTERNAL_STORAGE_PERMISSION);
                } else {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
            if (requestCode == RESULT_LOAD_IMG) {
                try {
                    final Uri imageUri = data.getData();
                    String imgUrl = getRealPathFromURI(getActivity(), imageUri);
                    File imageFile = new File(imgUrl);
                    changedProfileImageFile = new ParseFile(imageFile);
                    civProfileImage.setImageURI(imageUri);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Ошибка при выборе файла изображения",
                        Toast.LENGTH_SHORT).show();
                }
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_EXTERNAL_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
                } else {

                }
            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void loadProfile() {
        ParseUser object = ParseUser.getCurrentUser();

        String name = object.getString("name");
        String email = object.getString("email");
        String city = object.getString("city");

        if (name != null) {
            tvName.setText(name);
        } else {
            tvName.setText("Не имеется");
        }

        if (email != null) {
            tvEmail.setText(email);
        } else {
            tvEmail.setText("Не имеется");
        }

        if (city != null) {
            tvCity.setText(city);
        } else {
            tvCity.setText("Не выбран");
        }

        tvPhone.setText(ParseUser.getCurrentUser().getUsername());

        ParseFile profileImage = object.getParseFile("profileImage");
        if (profileImage != null) {
            Picasso.with(civProfileImage.getContext())
                .load(profileImage.getUrl())
                .resize(50, 50)
                .into(civProfileImage);
        }
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {

            }
        });
    }

    /*private String getPhoneNumber() {
        SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        return sPrefs.getString("PhoneNumber", null);
    }*/
}

