package kz.ikar.rinfo.fragments.restaurants;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.MainActivity;
import kz.ikar.rinfo.adapters.FilterPricesAdapter;
import kz.ikar.rinfo.adapters.FilterPricesAdapter.OnItemEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends Fragment {
    RecyclerView rvPrices;
    LinearLayout llFeatures;
    LinearLayout llNations;
    Button btnReady;

    public FilterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_filter, container, false);

        llFeatures = rootView.findViewById(R.id.ll_features);
        llNations = rootView.findViewById(R.id.ll_nations);

        int height = ((MainActivity)getActivity()).getStatusBarHeight();
        View statusBar = rootView.findViewById(R.id.status_bar);
        statusBar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height));

        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        btnReady = rootView.findViewById(R.id.btn_ready);
        btnReady.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).isReadyPressed = true;
                getActivity().onBackPressed();
            }
        });

        rvPrices = rootView.findViewById(R.id.rv_prices);
        rvPrices.setLayoutManager(new GridLayoutManager(getActivity(), 1, LinearLayoutManager.HORIZONTAL, false));

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FilterPricesAdapter adapter = new FilterPricesAdapter(Arrays.asList("от 5000 ₸", "5000 - 10 000 ₸", "от 10 000 ₸"),
            new OnItemEventListener() {
                @Override
                public void onItemClicked(String key) {

                }
            });
        rvPrices.setAdapter(adapter);

        llFeatures.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_main, new CuisineFeaturesFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        llNations.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_main, new CuisineNationsFragment(), "CurrentFragment");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }
}
