package kz.ikar.rinfo.fragments.restaurants;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.CompilationActivity;
import kz.ikar.rinfo.activities.FilterActivity;
import kz.ikar.rinfo.activities.RestaurantActivity;
import kz.ikar.rinfo.adapters.SearchRestaurantsListAdapter;
import kz.ikar.rinfo.models.Cuisine;
import kz.ikar.rinfo.models.Restaurant;

public class SearchFragment extends Fragment {
    ImageButton ibFilter;
    RecyclerView rvRestaurants;
    EditText etSearch;
    ProgressBar progressBar;
    TextView tvEmpty;

    SearchRestaurantsListAdapter adapter;

    int filterByPrice = -1;
    boolean filterIsOpen = false;
    int selectedPlace = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        ibFilter = rootView.findViewById(R.id.ib_filter);
        rvRestaurants = rootView.findViewById(R.id.rv_restaurants);
        etSearch = rootView.findViewById(R.id.et_search);
        progressBar = rootView.findViewById(R.id.progressBar);
        tvEmpty = rootView.findViewById(R.id.tv_empty);

        rvRestaurants.setLayoutManager(new GridLayoutManager(getActivity(), 1, LinearLayoutManager.VERTICAL, false));
        rvRestaurants.setNestedScrollingEnabled(false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ibFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(getActivity(), FilterActivity.class);
                activityIntent.putExtra("SelectedIndex", filterByPrice);
                startActivityForResult(activityIntent, 9);
            }
        });

        etSearch.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    loadRestaurants();

                    return true;
                }

                return false;
            }
        });
    }

    private void loadRestaurants() {
        loadingStarted();
        tvEmpty.setVisibility(View.GONE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                adapter = new SearchRestaurantsListAdapter(getRestaurants(false),
                    new SearchRestaurantsListAdapter.OnItemEventListener() {
                        @Override
                        public void onBottomReached(int position) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.datasetChanged(getRestaurants(true));
                                }
                            }).start();
                        }

                        @Override
                        public void onItemClicked(String restaurantId, int position) {
                            selectedPlace = position;
                            Intent activityIntent = new Intent(getActivity(),
                                RestaurantActivity.class);
                            activityIntent.putExtra("RestaurantId", restaurantId);
                            startActivityForResult(activityIntent, 1);
                        }

                        @Override
                        public void onItemFabClicked(final Restaurant restaurant,
                            FloatingActionButton fabLike, TextView tvLikes) {

                            ParseUser currentUser = ParseUser.getCurrentUser();
                            if (currentUser == null) {
                                Toast
                                    .makeText(getActivity(),
                                        "Необходимо авторизоваться", Toast.LENGTH_SHORT)
                                    .show();
                                return;
                            }

                            if (!restaurant.isFavorite()) {
                                fabLike.setImageDrawable(
                                    ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.favorite_filled));
                                int count = Integer.parseInt(tvLikes.getText().toString()) + 1;
                                tvLikes.setText("" + count);
                            } else {
                                fabLike.setImageDrawable(ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.like));
                                int count = Integer.parseInt(tvLikes.getText().toString()) - 1;
                                tvLikes.setText("" + count);
                            }
                            restaurant.setFavorite(!restaurant.isFavorite());

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        likeRestaurant(restaurant.getId());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    });

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        rvRestaurants.setAdapter(adapter);
                        if (adapter.getItemCount() == 0) {
                            tvEmpty.setVisibility(View.VISIBLE);
                        }
                        loadingFinished();
                    }
                });
            }
        }).start();
    }

    private boolean likeRestaurant(String restId) throws ParseException {
        ParseObject restObject = ParseQuery.getQuery("Place").get(restId);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Favorite");
        query.whereEqualTo("place", restObject);
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        int result = query.count();

        if (result != 0) {
            query.getFirst().delete();

            return false;
        } else {
            ParseObject favorite = new ParseObject("Favorite");
            favorite.put("user", ParseUser.getCurrentUser());
            favorite.put("place", restObject);
            favorite.save();

            return true;
        }
    }

    private void loadingStarted() {
        rvRestaurants.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void loadingFinished() {
        progressBar.setVisibility(View.GONE);
        rvRestaurants.setVisibility(View.VISIBLE);
    }

    private List<Restaurant> getRestaurants(boolean canSkip) {
        List<Restaurant> restaurants = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Place");
        query.include("schedule");
        if (etSearch.getText() != null && !etSearch.getText().toString().equals("")) {
            query.whereContains("title", etSearch.getText().toString());
        }
        if (filterByPrice != -1) {
            switch (filterByPrice) {
                case 0:
                    query.whereLessThanOrEqualTo("averageBill", 5000);
                    break;
                case 1:
                    query.whereGreaterThanOrEqualTo("averageBill", 5000);
                    query.whereLessThanOrEqualTo("averageBill", 10000);
                    break;
                case 2:
                    query.whereGreaterThanOrEqualTo("averageBill", 10000);
                    break;
            }
        }
        query.orderByAscending("order");
        if (Cuisine.GET_TAGS().size() != 0)
            query.whereContainedIn("cuisines", Cuisine.GET_TAGS());

        if (canSkip) {
            int skipCount = 10 * (adapter == null ? 0 : ((adapter.getItemCount() + 9) / 10));
            query.setSkip(skipCount);
        }

        List<ParseObject> objects = null;
        try {
            objects = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (objects != null) {
            for (ParseObject place : objects) {
                if (filterIsOpen) {
                    if (!isOpen(place.getParseObject("schedule"))) {
                        continue;
                    }
                }

                String address = place.getString("address"),
                    avgCost = "Ср. чек: " + place.get("averageBill") + " ₸",
                    title = place.getString("title"),
                    synopsis = place.getString("synopsis"),
                    shortDesc = place.getString("shortdesc");

                int favCount = place.getInt("favoriteCount");

                ParseFile file = place.getParseFile("image");

                if (address != null && title != null && synopsis != null && shortDesc != null && file != null) {
                    Restaurant restaurant = new Restaurant(
                        place.getObjectId(),
                        address,
                        avgCost,
                        title,
                        synopsis,
                        shortDesc,
                        favCount,
                        file.getUrl(),
                        place.getString("discountTitle"),
                        place.getString("discountDuration")
                    );

                    if (ParseUser.getCurrentUser() != null) {
                        ParseQuery<ParseObject> likeQuery = ParseQuery.getQuery("Favorite");
                        likeQuery.whereEqualTo("place", place);
                        likeQuery.whereEqualTo("user", ParseUser.getCurrentUser());
                        try {
                            restaurant.setFavorite(likeQuery.count() != 0);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            restaurant.setFavorite(false);
                        }
                    }
                    restaurants.add(restaurant);
                }
            }
        }

        return restaurants;
    }

    private boolean isOpen(ParseObject scheduleObject) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE", new Locale("en", "KZ"));
        String dayOfWeekStr = dateFormat.format(date).toLowerCase();

        try {
            scheduleObject.fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String workingHours = scheduleObject.getString(dayOfWeekStr);
        String[] hours = workingHours.split("-");

        int openHours = Integer.parseInt(hours[0]);
        int closeHours = Integer.parseInt(hours[1]);

        int currentHours = calendar.get(Calendar.HOUR_OF_DAY);
        int currentMinutes = calendar.get(Calendar.MINUTE);

        int currentMinutesOfDay = currentHours * 60 + currentMinutes;
        return currentMinutesOfDay >= openHours && currentMinutesOfDay <= closeHours;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == -1) {
            if (requestCode == 9) {
                filterByPrice = data.getIntExtra("SelectedValue", -1);
                filterIsOpen = data.getBooleanExtra("IsOpen", false);
                loadRestaurants();
            }
        }

        if (resultCode == 7 && selectedPlace != -1) {
            adapter.getmItemList()
                    .get(selectedPlace)
                    .setFavorite(
                            !adapter.getmItemList()
                                    .get(selectedPlace)
                                    .isFavorite()
                    );
            adapter.notifyItemChanged(selectedPlace);

            selectedPlace = -1;
        }
    }
}
