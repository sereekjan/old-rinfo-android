package kz.ikar.rinfo.fragments.profile;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import java.util.ArrayList;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.RestaurantActivity;
import kz.ikar.rinfo.adapters.RestaurantsListAdapter;
import kz.ikar.rinfo.adapters.RestaurantsListAdapter.OnItemEventListener;
import kz.ikar.rinfo.models.Restaurant;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritePlacesFragment extends Fragment {
    RecyclerView rvFavorites;
    RestaurantsListAdapter adapter;

    int selectedPlace = -1;

    public FavoritePlacesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_favorite_places, container, false);

        rvFavorites = rootView.findViewById(R.id.rv_favorites);

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1, LinearLayoutManager.VERTICAL, false);
        rvFavorites.setLayoutManager(manager);
        rvFavorites.setNestedScrollingEnabled(false);

        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        new Thread(new Runnable() {
            @Override
            public void run() {
                adapter = new RestaurantsListAdapter(loadFavoriteRestaurants(),
                    new OnItemEventListener() {
                        @Override
                        public void onBottomReached(int position) {
                            adapter.datasetChanged(loadFavoriteRestaurants());
                        }

                        @Override
                        public void onItemClicked(String restaurantId, int position) {
                            selectedPlace = position;
                            Intent activityIntent = new Intent(getActivity(),
                                RestaurantActivity.class);
                            activityIntent.putExtra("RestaurantId", restaurantId);
                            startActivityForResult(activityIntent, 1);
                        }

                        @Override
                        public void onItemFabClicked(final Restaurant restaurant,
                            FloatingActionButton fabLike,
                            TextView tvLikes) {

                            ParseUser currentUser = ParseUser.getCurrentUser();
                            if (currentUser == null) {
                                Toast
                                    .makeText(getActivity(),
                                        "Необходимо авторизоваться", Toast.LENGTH_SHORT)
                                    .show();
                                return;
                            }

                            if (!restaurant.isFavorite()) {
                                fabLike.setImageDrawable(
                                    ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.favorite_filled));
                                int count = Integer.parseInt(tvLikes.getText().toString()) + 1;
                                tvLikes.setText("" + count);
                            } else {
                                fabLike.setImageDrawable(ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.like));
                                int count = Integer.parseInt(tvLikes.getText().toString()) - 1;
                                tvLikes.setText("" + count);
                            }
                            restaurant.setFavorite(!restaurant.isFavorite());

                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        likeRestaurant(restaurant.getId());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    });

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        rvFavorites.setAdapter(adapter);
                    }
                });
            }
        }).start();
    }

    private boolean likeRestaurant(String restId) throws ParseException {
        ParseObject restObject = ParseQuery.getQuery("Place").get(restId);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Favorite");
        query.whereEqualTo("place", restObject);
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        int result = query.count();

        if (result != 0) {
            query.getFirst().delete();

            return false;
        } else {
            ParseObject favorite = new ParseObject("Favorite");
            favorite.put("user", ParseUser.getCurrentUser());
            favorite.put("place", restObject);
            favorite.save();

            return true;
        }
    }

    private List<Restaurant> loadFavoriteRestaurants() {
        List<Restaurant> restaurants = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Favorite");
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        query.include("place");
        query.setLimit(10);

        int skipCount = 10 * (adapter == null ? 0 : ((adapter.getItemCount() + 9) / 10));
        query.setSkip(skipCount);

        List<ParseObject> objects = null;
        try {
            objects = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (objects != null) {
            for (ParseObject object : objects) {
                ParseObject place = object.getParseObject("place");
                if (place != null) {
                    String address = place.getString("address"),
                        avgCost = "Ср. чек: " + place.get("averageBill") + " ₸",
                        title = place.getString("title"),
                        synopsis = place.getString("synopsis"),
                        shortDesc = place.getString("shortdesc");

                    int favCount = place.getInt("favoriteCount");

                    ParseFile file = place.getParseFile("image");

                    if (address != null && title != null && synopsis != null && shortDesc != null
                        && file != null) {
                        Restaurant restaurant = new Restaurant(
                            place.getObjectId(),
                            address,
                            avgCost,
                            title,
                            synopsis,
                            shortDesc,
                            favCount,
                            file.getUrl(),
                            place.getString("discountTitle"),
                            place.getString("discountDuration")
                        );

                        if (ParseUser.getCurrentUser() != null) {
                            ParseQuery<ParseObject> likeQuery = ParseQuery.getQuery("Favorite");
                            likeQuery.whereEqualTo("place", place);
                            likeQuery.whereEqualTo("user", ParseUser.getCurrentUser());
                            try {
                                restaurant.setFavorite(likeQuery.count() != 0);
                            } catch (ParseException e) {
                                e.printStackTrace();
                                restaurant.setFavorite(false);
                            }
                        }
                        restaurants.add(restaurant);
                    }
                }
            }
        }

        return restaurants;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 7 && selectedPlace != -1) {
            adapter.getmItemList()
                    .get(selectedPlace)
                    .setFavorite(
                            !adapter.getmItemList()
                                    .get(selectedPlace)
                                    .isFavorite()
                    );
            adapter.notifyItemChanged(selectedPlace);

            selectedPlace = -1;
        }
    }
}
