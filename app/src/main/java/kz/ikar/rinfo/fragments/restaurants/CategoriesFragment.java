package kz.ikar.rinfo.fragments.restaurants;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.util.ArrayList;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.CompilationActivity;
import kz.ikar.rinfo.adapters.CategoriesAdapter;
import kz.ikar.rinfo.adapters.CategoriesAdapter.OnItemEventListener;
import kz.ikar.rinfo.models.Category;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {

    private RecyclerView rvCategories;
    TextView tvNoConnection;
    SwipeRefreshLayout srlUpdate;
    CategoriesAdapter adapter;

    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        rvCategories = (RecyclerView) view.findViewById(R.id.rv_categories);
        tvNoConnection = view.findViewById(R.id.tv_no_connection);
        srlUpdate = view.findViewById(R.id.srl_update);

        rvCategories.setLayoutManager(new GridLayoutManager(getActivity(), 1, LinearLayoutManager.VERTICAL, false));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        srlUpdate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet(getActivity())) {
                    adapter = null;
                    loadCategoriesAsync();
                    tvNoConnection.setVisibility(View.GONE);
                    rvCategories.setVisibility(View.VISIBLE);
                } else {
                    rvCategories.setVisibility(View.GONE);
                    tvNoConnection.setVisibility(View.VISIBLE);
                    srlUpdate.setRefreshing(false);
                }
            }
        });

        if (isConnectedToInternet(getActivity())) {
            adapter = null;
            loadCategoriesAsync();
        } else {
            rvCategories.setVisibility(View.GONE);
            tvNoConnection.setVisibility(View.VISIBLE);
        }
    }

    public static boolean isConnectedToInternet(Context context) {

        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private void loadCategoriesAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (adapter == null) {
                    adapter = new CategoriesAdapter(getCategories(),
                        new OnItemEventListener() {
                            @Override
                            public void onBottomReached(int position) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.datasetChanged(getCategories());
                                    }
                                }).start();
                            }

                            @Override
                            public void onItemClicked(String id) {
                                Intent activityIntent = new Intent(getActivity(),
                                    CompilationActivity.class);
                                activityIntent.putExtra("CompilationId", id);
                                startActivity(activityIntent);
                            }
                        });
                }

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        rvCategories.setAdapter(adapter);
                        srlUpdate.setRefreshing(false);
                    }
                });
            }
        }).start();
    }

    private List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Compilation");
        query.setLimit(10);

        int skipCount = 10 * (adapter == null ? 0 : ((adapter.getItemCount() + 9) / 10));
        query.setSkip(skipCount);

        List<ParseObject> objects = null;
        try {
            objects = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (objects != null) {
            for (ParseObject compilation : objects) {
                Category category = new Category(
                    compilation.getObjectId(),
                    compilation.getString("title"),
                    compilation.getInt("numberOfPlace"),
                    compilation.getParseFile("image").getUrl()
                );

                categories.add(category);
            }
        }

        return categories;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
