package kz.ikar.rinfo.fragments.profile;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.OrdersAdapter;
import kz.ikar.rinfo.models.Order;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryFragment extends Fragment {
    RecyclerView rvNewOrders, rvOldOrders;
    OrdersAdapter newOrdersAdapter, oldOrdersAdapter;

    public OrderHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_order_history, container, false);

        rvNewOrders = rootView.findViewById(R.id.rv_new_orders);
        rvOldOrders = rootView.findViewById(R.id.rv_old_orders);

        rvNewOrders.setNestedScrollingEnabled(false);
        rvOldOrders.setNestedScrollingEnabled(false);

        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvNewOrders.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvOldOrders.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    newOrdersAdapter = new OrdersAdapter(getRecentOrders());
                    Handler mainHandler = new Handler(Looper.getMainLooper());
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            rvNewOrders.setAdapter(newOrdersAdapter);
                        }
                    });
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    oldOrdersAdapter = new OrdersAdapter(getOldOrders());
                    Handler mainHandler = new Handler(Looper.getMainLooper());
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            rvOldOrders.setAdapter(oldOrdersAdapter);
                        }
                    });
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private List<Order> getOldOrders() throws ParseException {
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Date date = new Date(System.currentTimeMillis() - (7 * DAY_IN_MS));
        return getOrders(null, date);
    }

    private List<Order> getRecentOrders() throws ParseException {
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Date date = new Date(System.currentTimeMillis() - (7 * DAY_IN_MS));
        return getOrders(date, null);
    }

    private List<Order> getOrders(Date sinceDate, Date toDate) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Reservation");
        query.whereEqualTo("user", ParseUser.getCurrentUser());

        if (sinceDate != null) {
            query.whereGreaterThanOrEqualTo("updatedAt", sinceDate);
        }

        if (toDate != null) {
            query.whereLessThanOrEqualTo("updatedAt", toDate);
        }

        query.include("place");
        List<Order> orders = new ArrayList<>();
        List<ParseObject> result = query.find();
        for(ParseObject orderObject: result) {
            ParseObject placeObject = orderObject.getParseObject("place");
            if (placeObject == null) {
                continue;
            }
            ParseFile imgFile = placeObject.getParseFile("image");
            int peopleCount = orderObject.getNumber("numberOfPersons").intValue();
            Date date = orderObject.getDate("date");
            String time = orderObject.getString("time");
            String status = orderObject.getString("status");

            SimpleDateFormat format = new SimpleDateFormat("dd MMM",
                new Locale("ru", "RU"));
            String details = format.format(date);
            details += ", " + time;
            details += " на " + peopleCount + (peopleCount < 5 ? " человека" : " человек");

            Order order = new Order(
                orderObject.getObjectId(),
                placeObject.getString("title"),
                imgFile != null ? imgFile.getUrl() : null,
                details,
                status.equals("pending") ? "Ожидает подтверждения" : "Подтверждено"
            );
            orders.add(order);
        }
        return orders;
    }
}
