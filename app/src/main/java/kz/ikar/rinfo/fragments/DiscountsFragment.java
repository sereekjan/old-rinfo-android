package kz.ikar.rinfo.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.MainActivity;
import kz.ikar.rinfo.adapters.TabViewPagerAdapter;
import kz.ikar.rinfo.fragments.discounts.DiscountsListFragment;
import kz.ikar.rinfo.fragments.discounts.EventsListFragment;
import kz.ikar.rinfo.fragments.restaurants.SearchFragment;

public class DiscountsFragment extends Fragment {
    ViewPager viewPager;
    TabLayout tabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_discounts, container, false);

        viewPager = view.findViewById(R.id.viewpager);
        tabLayout = view.findViewById(R.id.tab);

        int height = ((MainActivity)getActivity()).getStatusBarHeight();
        View statusBar = view.findViewById(R.id.status_bar);
        statusBar.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height));

        setupViewPager();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void setupViewPager() {
        TabViewPagerAdapter adapter = new TabViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new DiscountsListFragment(), "Акции");
        adapter.addFragment(new EventsListFragment(), "События");
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
