package kz.ikar.rinfo.fragments.discounts;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.util.ArrayList;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.EventActivity;
import kz.ikar.rinfo.adapters.EventsAdapter;
import kz.ikar.rinfo.adapters.EventsAdapter.OnItemEventListener;
import kz.ikar.rinfo.models.Event;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsListFragment extends Fragment {
    RecyclerView rvEvents;
    TextView tvNoConnection;
    SwipeRefreshLayout srlUpdate;

    public static final String TAG = "RINFO";

    private EventsAdapter adapter;

    public EventsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_discounts_list, container, false);

        rvEvents = (RecyclerView) view.findViewById(R.id.rv_discounts);
        tvNoConnection = view.findViewById(R.id.tv_no_connection);
        srlUpdate = view.findViewById(R.id.srl_update);

        rvEvents.setLayoutManager(new GridLayoutManager(getActivity(), 1, LinearLayoutManager.VERTICAL, false));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        srlUpdate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet(getActivity())) {
                    adapter = null;
                    loadDiscountsAsync();
                    tvNoConnection.setVisibility(View.GONE);
                    rvEvents.setVisibility(View.VISIBLE);
                } else {
                    rvEvents.setVisibility(View.GONE);
                    tvNoConnection.setVisibility(View.VISIBLE);
                    srlUpdate.setRefreshing(false);
                }
            }
        });

        if (isConnectedToInternet(getActivity())) {
            adapter = null;
            loadDiscountsAsync();
        } else {
            rvEvents.setVisibility(View.GONE);
            tvNoConnection.setVisibility(View.VISIBLE);
        }
    }

    public static boolean isConnectedToInternet(Context context) {

        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private void loadDiscountsAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Event> events = getEvents();
                adapter = new EventsAdapter(events,
                    new OnItemEventListener() {
                        @Override
                        public void onBottomReached(int position) {
                            UpdateListAsyncTask asyncTask = new UpdateListAsyncTask();
                            asyncTask.execute();
                            //adapter.datasetChanged(events);
                        }

                        @Override
                        public void onItemClicked(String id) {
                            Intent intent = new Intent(getContext(), EventActivity.class);
                            intent.putExtra("EventId", id);
                            startActivity(intent);
                        }
                    });

                Handler handler = new Handler(getActivity().getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        rvEvents.setAdapter(adapter);
                        srlUpdate.setRefreshing(false);
                    }
                });
            }
        }).start();
    }

    private List<Event> getEvents() {
        List<Event> events = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.include("place");
        query.setLimit(10);

        int skipCount = 10 * (adapter == null ? 0 : ((adapter.getItemCount() + 9) / 10));
        query.setSkip(skipCount);

        List<ParseObject> objects = null;
        try {
            objects = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (objects != null) {
            for (ParseObject eventParseObject : objects) {

                Event event = new Event(
                    eventParseObject.getObjectId(),
                    eventParseObject.getParseObject("place").getString("title"),
                    eventParseObject.getString("dueToTitle"),
                    eventParseObject.getString("title"),
                    eventParseObject.getString("fullDescription"),
                    eventParseObject.getParseFile("image").getUrl()
                );

                events.add(event);
            }
        }

        return events;
    }

    private class UpdateListAsyncTask extends AsyncTask<Integer, Void, List<Event>> {

        @Override
        protected List<Event> doInBackground(Integer... integers) {
            return getEvents();
        }

        @Override
        protected void onPostExecute(List<Event> events) {
            adapter.datasetChanged(events);
        }
    }
}
