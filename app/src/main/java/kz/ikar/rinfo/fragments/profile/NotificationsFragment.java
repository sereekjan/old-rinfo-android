package kz.ikar.rinfo.fragments.profile;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.NotificationsAdapter;
import kz.ikar.rinfo.models.Notification;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends Fragment {
    RecyclerView rvNotifications;

    NotificationsAdapter adapter;

    public NotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notifications, container, false);

        rvNotifications = rootView.findViewById(R.id.rv_notifications);

        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvNotifications.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    adapter = new NotificationsAdapter(getNotifications());
                    Handler mainHandler = new Handler(Looper.getMainLooper());
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            rvNotifications.setAdapter(adapter);
                        }
                    });
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private List<Notification> getNotifications() throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Notification");
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        query.include("reservation");
        query.include("place");
        query.orderByDescending("createdAt");
        List<Notification> notifications = new ArrayList<>();
        List<ParseObject> result = query.find();
        for(ParseObject notifObject: result) {
            ParseObject reservObject = notifObject.getParseObject("reservation");
            ParseObject placeObject = null;
            if (reservObject != null) {
                placeObject = reservObject.getParseObject("place");
            }
            ParseFile imgFile = null;
            if (placeObject != null) {
                placeObject.fetchIfNeeded();
                imgFile = placeObject.getParseFile("image");
            }
            Notification notification = new Notification(
                notifObject.getObjectId(),
                notifObject.getString("title"),
                getTimeAgo(notifObject.getUpdatedAt().getTime(), getActivity()),
                imgFile != null ? imgFile.getUrl() : null);
            notifications.add(notification);
        }
        return notifications;
    }

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static String getTimeAgo(long time, Context ctx) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = new Date().getTime();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "Только что";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "Минуту назад";
        } else if (diff < 50 * MINUTE_MILLIS) {
            if (diff / MINUTE_MILLIS > 10 && diff / MINUTE_MILLIS < 20)
                return diff / MINUTE_MILLIS + " минут назад";
            if ((diff / MINUTE_MILLIS) % 10 == 1)
                return diff / MINUTE_MILLIS + " минута назад";
            if (diff / MINUTE_MILLIS % 10 > 1 && diff / MINUTE_MILLIS % 10 < 5) {
                return diff / MINUTE_MILLIS + " минуты назад";
            }
            return diff / MINUTE_MILLIS + " минут назад";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "Час назад";
        } else if (diff < 24 * HOUR_MILLIS) {
            if (diff / HOUR_MILLIS > 10 && diff / HOUR_MILLIS < 20)
                return diff / HOUR_MILLIS + " часов назад";
            if ((diff / HOUR_MILLIS) % 10 == 1)
                return diff / HOUR_MILLIS + " час назад";
            if (diff / HOUR_MILLIS % 10 > 1 && diff / HOUR_MILLIS % 10 < 5) {
                return diff / HOUR_MILLIS + " часа назад";
            }
            return diff / HOUR_MILLIS + " часов назад";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "Вчера";
        } else {
            if (diff / DAY_MILLIS > 10 && diff / DAY_MILLIS < 20)
                return diff / DAY_MILLIS + " дней назад";
            if ((diff / DAY_MILLIS) % 10 == 1)
                return diff / DAY_MILLIS + " день назад";
            if (diff / DAY_MILLIS % 10 > 1 && diff / DAY_MILLIS % 10 < 5) {
                return diff / DAY_MILLIS + " дня назад";
            }
            return diff / DAY_MILLIS + " дней назад";
        }
    }
}
