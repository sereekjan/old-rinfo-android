package kz.ikar.rinfo.fragments.restaurants;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import java.util.ArrayList;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.RestaurantActivity;
import kz.ikar.rinfo.adapters.RestaurantsListAdapter;
import kz.ikar.rinfo.adapters.RestaurantsListAdapter.OnItemEventListener;
import kz.ikar.rinfo.models.Restaurant;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {

    RecyclerView rvRestaurants;
    TextView tvNoConnection;
    SwipeRefreshLayout srlUpdate;

    public static final String TAG = "RINFO";
    int selectedPlace = -1;

    RestaurantsListAdapter adapter;

    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        rvRestaurants = (RecyclerView) view.findViewById(R.id.rv_restaurants);
        tvNoConnection = view.findViewById(R.id.tv_no_connection);
        srlUpdate = view.findViewById(R.id.srl_update);

        final GridLayoutManager manager = new GridLayoutManager(getActivity(), 1, LinearLayoutManager.VERTICAL, false);
        rvRestaurants.setLayoutManager(manager);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        srlUpdate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet(getActivity())) {
                    adapter = null;
                    loadRestaurantsAsync();
                    tvNoConnection.setVisibility(View.GONE);
                    rvRestaurants.setVisibility(View.VISIBLE);
                } else {
                    rvRestaurants.setVisibility(View.GONE);
                    tvNoConnection.setVisibility(View.VISIBLE);
                    srlUpdate.setRefreshing(false);
                }
            }
        });

        if (isConnectedToInternet(getActivity())) {
            adapter = null;
            loadRestaurantsAsync();
        } else {
            rvRestaurants.setVisibility(View.GONE);
            tvNoConnection.setVisibility(View.VISIBLE);
        }
    }

    public static boolean isConnectedToInternet(Context context) {

        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private void loadRestaurantsAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                adapter = new RestaurantsListAdapter(getRestaurants(),
                    new OnItemEventListener() {
                        @Override
                        public void onBottomReached(int position) {
                            UpdateListAsyncTask asyncTask = new UpdateListAsyncTask();
                            asyncTask.execute();
                        }

                        @Override
                        public void onItemClicked(String restaurantId, int position) {
                            selectedPlace = position;
                            Intent activityIntent = new Intent(getActivity(),
                                RestaurantActivity.class);
                            activityIntent.putExtra("RestaurantId", restaurantId);
                            startActivityForResult(activityIntent, 1);
                        }

                        @Override
                        public void onItemFabClicked(final Restaurant restaurant,
                            FloatingActionButton fabLike, TextView tvLikes) {

                            ParseUser currentUser = ParseUser.getCurrentUser();
                            if (currentUser == null) {
                                Toast
                                    .makeText(getActivity(),
                                        "Необходимо авторизоваться", Toast.LENGTH_SHORT)
                                    .show();
                                return;
                            }

                            if (!restaurant.isFavorite()) {
                                fabLike.setImageDrawable(
                                    ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.favorite_filled));
                                int count = Integer.parseInt(tvLikes.getText().toString()) + 1;
                                tvLikes.setText("" + count);
                            } else {
                                fabLike.setImageDrawable(ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.like));
                                int count = Integer.parseInt(tvLikes.getText().toString()) - 1;
                                tvLikes.setText("" + count);
                            }
                            restaurant.setFavorite(!restaurant.isFavorite());

                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        likeRestaurant(restaurant.getId());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    });
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        rvRestaurants.setAdapter(adapter);
                        srlUpdate.setRefreshing(false);
                    }
                });
            }
        }).start();
    }

    private boolean likeRestaurant(String restId) throws ParseException {
        ParseObject restObject = ParseQuery.getQuery("Place").get(restId);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Favorite");
        query.whereEqualTo("place", restObject);
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        int result = query.count();

        if (result != 0) {
            query.getFirst().delete();

            return false;
        } else {
            ParseObject favorite = new ParseObject("Favorite");
            favorite.put("user", ParseUser.getCurrentUser());
            favorite.put("place", restObject);
            favorite.save();

            return true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 7 && selectedPlace != -1) {
            adapter.getmItemList()
                    .get(selectedPlace)
                    .setFavorite(
                            !adapter.getmItemList()
                            .get(selectedPlace)
                            .isFavorite()
                    );
            adapter.notifyItemChanged(selectedPlace);

            selectedPlace = -1;
        }
    }

    private List<Restaurant> getRestaurants() {
        List<Restaurant> restaurants = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Place");
        query.orderByAscending("order");

        int k = 0;
        try {
            k = query.count();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            k++;
        }
        query.setLimit(10);

        int skipCount = 10 * (adapter == null ? 0 : ((adapter.getItemCount() + 9) / 10));
        query.setSkip(skipCount);

        List<ParseObject> objects = null;
        try {
            objects = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (objects != null) {
            for (ParseObject place : objects) {
                String address = place.getString("address"),
                    avgCost = "Ср. чек: " + place.get("averageBill") + " ₸",
                    title = place.getString("title"),
                    synopsis = place.getString("synopsis"),
                    shortDesc = place.getString("shortdesc");

                int favCount = place.getInt("favoriteCount");

                ParseFile file = place.getParseFile("image");

                if (address != null && title != null && synopsis != null && shortDesc != null && file != null) {
                    Restaurant restaurant = new Restaurant(
                        place.getObjectId(),
                        address,
                        avgCost,
                        title,
                        synopsis,
                        shortDesc,
                        favCount,
                        file.getUrl(),
                        place.getString("discountTitle"),
                        place.getString("discountDuration")
                    );

                    if (ParseUser.getCurrentUser() != null) {
                        ParseQuery<ParseObject> likeQuery = ParseQuery.getQuery("Favorite");
                        likeQuery.whereEqualTo("place", place);
                        likeQuery.whereEqualTo("user", ParseUser.getCurrentUser());
                        try {
                            restaurant.setFavorite(likeQuery.count() != 0);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            restaurant.setFavorite(false);
                        }
                    }
                    restaurants.add(restaurant);
                }
            }
        }

        return restaurants;
    }

    private class UpdateListAsyncTask extends AsyncTask<Integer, Void, List<Restaurant>> {

        @Override
        protected List<Restaurant> doInBackground(Integer... integers) {
            return getRestaurants();
        }

        @Override
        protected void onPostExecute(List<Restaurant> restaurants) {
            adapter.datasetChanged(restaurants);
        }
    }
}
