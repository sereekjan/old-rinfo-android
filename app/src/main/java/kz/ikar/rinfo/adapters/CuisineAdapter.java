package kz.ikar.rinfo.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.CuisineAdapter.CuisineViewHolder;
import kz.ikar.rinfo.models.Cuisine;

/**
 * Created by User on 08.02.2018.
 */

public class CuisineAdapter extends Adapter<CuisineViewHolder> {
    private List<Cuisine> mItemList;

    public CuisineAdapter(List<Cuisine> mItemList) {
        this.mItemList = mItemList;
        this.mItemList.add(0, new Cuisine(3500));
    }

    @Override
    public CuisineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_cuisine, parent, false);

        return new CuisineViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(CuisineViewHolder holder, int position) {
        holder.bind(mItemList.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    static class CuisineViewHolder extends ViewHolder {
        ImageView ivLogo;
        TextView tvTitle;
        TextView tvAvgCost;
        LinearLayout llCuisine;
        LinearLayout llCost;

        public CuisineViewHolder(View itemView) {
            super(itemView);

            ivLogo = itemView.findViewById(R.id.iv_logo);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAvgCost = itemView.findViewById(R.id.tv_avg_cost);
            llCuisine = itemView.findViewById(R.id.ll_cuisine);
            llCost = itemView.findViewById(R.id.ll_price);
        }

        public void bind(Cuisine cuisine, int position) {
            if (cuisine.getTag() == null) {
                llCuisine.setVisibility(View.GONE);
                llCost.setVisibility(View.VISIBLE);
                tvAvgCost.setText("" + cuisine.getPrice());
            } else {
                llCuisine.setVisibility(View.VISIBLE);
                llCost.setVisibility(View.GONE);
                ivLogo.setImageDrawable(ContextCompat.getDrawable(ivLogo.getContext(), cuisine.getMipmapId()));
                tvTitle.setText(cuisine.getTitle());
            }
        }
    }
}
