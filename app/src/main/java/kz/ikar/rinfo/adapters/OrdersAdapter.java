package kz.ikar.rinfo.adapters;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.OrdersAdapter.OrdersViewHolder;
import kz.ikar.rinfo.models.Order;

/**
 * Created by User on 07.03.2018.
 */

public class OrdersAdapter extends Adapter<OrdersViewHolder> {
    List<Order> mItemList;

    public OrdersAdapter(List<Order> mItemList) {
        this.mItemList = mItemList;
    }

    @Override
    public OrdersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_history, parent, false);

        return new OrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrdersViewHolder holder, int position) {
        holder.bind(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    static class OrdersViewHolder extends ViewHolder {
        CircleImageView civPlaceImg;
        TextView tvPlaceTitle;
        TextView tvDetails;
        TextView tvStatus;

        public OrdersViewHolder(View itemView) {
            super(itemView);

            civPlaceImg = itemView.findViewById(R.id.civ_place_img);
            tvPlaceTitle = itemView.findViewById(R.id.tv_place_title);
            tvDetails = itemView.findViewById(R.id.tv_details);
            tvStatus = itemView.findViewById(R.id.tv_status);
        }

        public void bind(Order order) {
            if (order.getPlaceImgUrl() != null) {
                Picasso.with(civPlaceImg.getContext())
                    .load(order.getPlaceImgUrl())
                    .resize(50, 50)
                    .centerCrop()
                    .into(civPlaceImg);
            }

            tvPlaceTitle.setText(order.getPlaceTitle());
            tvDetails.setText(order.getDetails());
            tvStatus.setText(order.getStatus());
        }
    }
}
