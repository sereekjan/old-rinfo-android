package kz.ikar.rinfo.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.ReviewsAdapter.ReviewsViewHolder;
import kz.ikar.rinfo.models.Review;

/**
 * Created by User on 08.02.2018.
 */

public class ReviewsAdapter extends Adapter<ReviewsViewHolder> {
    List<Review> mItemList;

    public ReviewsAdapter(List<Review> mItemList) {
        this.mItemList = mItemList;
    }

    @Override
    public ReviewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReviewsViewHolder(
            LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review, parent, false));
    }

    @Override
    public void onBindViewHolder(ReviewsViewHolder holder, int position) {
        holder.bind(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    static class ReviewsViewHolder extends ViewHolder {
        TextView tvUsername;
        TextView tvDate;
        ImageView ivState;
        TextView tvText;

        public ReviewsViewHolder(View itemView) {
            super(itemView);

            tvUsername = itemView.findViewById(R.id.tv_username);
            tvDate = itemView.findViewById(R.id.tv_date);
            ivState = itemView.findViewById(R.id.iv_state);
            tvText = itemView.findViewById(R.id.tv_text);
        }

        public void bind(Review review) {
            tvUsername.setText(review.getUsername());
            tvDate.setText(review.getDate());
            if (review.isPositive()) {
                ivState.setImageDrawable(ContextCompat.getDrawable(ivState.getContext(), R.mipmap.thumb_up));
            } else {
                ivState.setImageDrawable(ContextCompat.getDrawable(ivState.getContext(), R.mipmap.thumb_down));
            }
            tvText.setText(review.getText());
        }
    }
}
