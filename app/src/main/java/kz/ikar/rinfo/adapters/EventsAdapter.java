package kz.ikar.rinfo.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.DiscountsAdapter.DiscountsViewHolder;
import kz.ikar.rinfo.adapters.EventsAdapter.EventsViewHolder;
import kz.ikar.rinfo.models.Discount;
import kz.ikar.rinfo.models.Event;

/**
 * Created by User on 08.01.2018.
 */

public class EventsAdapter extends Adapter<EventsViewHolder> {
    private List<Event> mItemList;
    OnItemEventListener onItemEventListener;

    public interface OnItemEventListener {
        void onBottomReached(int position);
        void onItemClicked(String eventId);
    }

    public EventsAdapter(List<Event> mItemList, OnItemEventListener listener) {
        this.mItemList = mItemList;
        this.onItemEventListener = listener;
    }

    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_event, parent, false);

        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsViewHolder holder, int position) {
        holder.bind(mItemList.get(position), onItemEventListener);

        if (position == mItemList.size() - 1/* && position % 10 == 9*/) {
            onItemEventListener.onBottomReached(position);
        }
    }

    // is called asynchronously
    public void datasetChanged(final List<Event> dataset) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                mItemList.addAll(dataset);
                EventsAdapter.this.notifyItemRangeChanged(mItemList.size() - dataset.size() - 1, dataset.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    public static class EventsViewHolder extends RecyclerView.ViewHolder {
        CardView cvTile;
        ImageView ivPhoto;
        TextView tvTime;
        TextView tvTitle;
        TextView tvCount;
        TextView tvDescription;
        TextView tvSubDescription;

        public EventsViewHolder(View itemView) {
            super(itemView);

            cvTile = (CardView) itemView.findViewById(R.id.cv_tile);
            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_back);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvCount = (TextView) itemView.findViewById(R.id.tv_count);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_desc);
            tvSubDescription = (TextView) itemView.findViewById(R.id.tv_subdesc);
        }

        public void bind(final Event event, final OnItemEventListener listener) {
            Picasso.with(ivPhoto.getContext())
                .load(event.getLogoUrl())
                .placeholder(ContextCompat.getDrawable(ivPhoto.getContext(), R.drawable.image_placeholder))
                .fit().centerCrop()
                .into(ivPhoto);
            tvTime.setText(event.getTime());
            tvTitle.setText(event.getTitle());
            tvDescription.setText(event.getDescription());
            tvSubDescription.setText(event.getSubDescription());

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(event.getId());
                }
            });
        }
    }
}
