package kz.ikar.rinfo.adapters;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import java.util.Map;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.FilterPricesAdapter.FilterPricesViewHolder;

/**
 * Created by User on 14.02.2018.
 */

public class FilterPricesAdapter extends Adapter<FilterPricesViewHolder> {
    private List<String> mItemList;
    private OnItemEventListener listener;
    public int indexSelected = -1;

    public interface OnItemEventListener {
        void onItemClicked(String key);
    }

    public FilterPricesAdapter(List<String> mItemList,
        OnItemEventListener listener) {
        this.mItemList = mItemList;
        this.listener = listener;
    }

    @Override
    public FilterPricesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_price_filter, parent, false);

        return new FilterPricesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FilterPricesViewHolder holder, final int position) {
        //holder.bind(mItemList.get(position), listener);

        if (indexSelected != -1 && position == indexSelected) {
            holder.tvPrice.setTextColor(Color.WHITE);
            holder.cvBack.setCardBackgroundColor(
                ContextCompat.getColor(holder.cvBack.getContext(), R.color.colorAccent));
        } else {
            holder.tvPrice.setTextColor(
                ContextCompat.getColor(holder.tvPrice.getContext(), R.color.colorTextDefault));
            holder.cvBack.setCardBackgroundColor(
                ContextCompat.getColor(holder.cvBack.getContext(), R.color.colorBackground2));
        }
        holder.tvPrice.setText(mItemList.get(position));

        holder.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == indexSelected) {
                    /*holder.tvPrice.setTextColor(
                        ContextCompat.getColor(holder.tvPrice.getContext(), R.color.colorTextDefault));
                    holder.cvBack.setBackgroundColor(
                        ContextCompat.getColor(holder.cvBack.getContext(), R.color.colorBackground2));
*/
                    indexSelected = -1;
                } else {
                    /*holder.tvPrice.setTextColor(Color.WHITE);
                    holder.cvBack.setBackgroundColor(
                        ContextCompat.getColor(holder.cvBack.getContext(), R.color.colorAccent));
*/
                    //int oldIndex = indexSelected;
                    indexSelected = position;
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    static class FilterPricesViewHolder extends ViewHolder {
        TextView tvPrice;
        CardView cvBack;

        public FilterPricesViewHolder(View itemView) {
            super(itemView);

            tvPrice = itemView.findViewById(R.id.tv_price);
            cvBack = itemView.findViewById(R.id.cv_back);
        }

        /*public void bind(final String value, final OnItemEventListener listener) {
            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(value);
                }
            });
            if (value) {
                tvPrice.setTextColor(
                    ContextCompat.getColor(tvPrice.getContext(), R.color.colorTextDefault));
                cvBack.setBackgroundColor(ContextCompat.getColor(cvBack.getContext(), R.color.colorBackground2));
            } else {
                tvPrice.setTextColor(Color.WHITE);
                cvBack.setBackgroundColor(ContextCompat.getColor(cvBack.getContext(), R.color.colorAccent));
            }
        }*/
    }
}
