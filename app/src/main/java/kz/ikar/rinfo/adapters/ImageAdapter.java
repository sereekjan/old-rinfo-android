package kz.ikar.rinfo.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.squareup.picasso.Picasso;

import java.util.List;

import kz.ikar.rinfo.R;
import kz.ikar.rinfo.helpers.TouchImageView;

/**
 * Created by User on 18.03.2018.
 */

public class ImageAdapter extends PagerAdapter {
    private String[] mImageBmp = new String[]{};

    public ImageAdapter(List<String> mImageBmp) {
        this.mImageBmp = mImageBmp.toArray(new String[mImageBmp.size()]);
    }

    @Override
    public int getCount() {
        if (mImageBmp == null)
            return 0;

        return mImageBmp.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View rootView = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_slider, container, false);

        ImageView touchImageView = rootView.findViewById(R.id.touch_image_view);
        //touchImageView.setScaleType(ScaleType.CENTER_CROP);
        //container.addView(touchImageView, 0);
        //touchImageView.setImageBitmap(mImageBmp[position]);
        Picasso.with(container.getContext())
                .load(mImageBmp[position])
                .fit()
                .centerInside()
                .into(touchImageView);

        container.addView(rootView);
        return rootView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        //((TouchImageView)container.getRootView().findViewById(R.id.touch_image_view));
    }


}
