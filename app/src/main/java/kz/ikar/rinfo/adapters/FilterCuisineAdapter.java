package kz.ikar.rinfo.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.FilterCuisineAdapter.FilterCuisineViewHolder;
import kz.ikar.rinfo.models.Cuisine;

/**
 * Created by User on 08.02.2018.
 */

public class FilterCuisineAdapter extends Adapter<FilterCuisineViewHolder> {
    private List<Cuisine> mItemList;
    private OnItemEventListener listener;

    public interface OnItemEventListener {
        void onItemClicked(String tag, boolean isChecked);
    }

    public FilterCuisineAdapter(List<Cuisine> mItemList, OnItemEventListener listener) {
        this.mItemList = mItemList;
        this.listener = listener;
    }

    @Override
    public FilterCuisineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_filter_cuisine, parent, false);

        return new FilterCuisineViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(FilterCuisineViewHolder holder, int position) {
        holder.bind(mItemList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    static class FilterCuisineViewHolder extends ViewHolder {
        ImageView ivLogo;
        TextView tvTitle;
        ImageView ivSelected;

        public FilterCuisineViewHolder(View itemView) {
            super(itemView);

            ivLogo = itemView.findViewById(R.id.iv_logo);
            tvTitle = itemView.findViewById(R.id.tv_cuisine_title);
            ivSelected = itemView.findViewById(R.id.iv_selected);
        }

        public void bind(final Cuisine cuisine, final OnItemEventListener listener) {
            ivLogo.setImageDrawable(ContextCompat.getDrawable(ivLogo.getContext(), cuisine.getMipmapId()));
            tvTitle.setText(cuisine.getTitle());

            ivSelected.setVisibility(cuisine.isChecked() ? View.VISIBLE : View.INVISIBLE);

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cuisine.isChecked()) {
                        ivSelected.setVisibility(View.INVISIBLE);
                        //listener.onItemClicked(cuisine.getTag(), false);
                    } else {
                        ivSelected.setVisibility(View.VISIBLE);
                        //listener.onItemClicked(cuisine.getTag(), true);
                    }
                    cuisine.setChecked(!cuisine.isChecked());
                }
            });
        }
    }
}
