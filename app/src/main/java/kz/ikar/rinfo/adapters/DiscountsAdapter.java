package kz.ikar.rinfo.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.DiscountsAdapter.DiscountsViewHolder;
import kz.ikar.rinfo.models.Discount;

/**
 * Created by User on 08.01.2018.
 */

public class DiscountsAdapter extends Adapter<DiscountsViewHolder> {
    private List<Discount> mItemList;
    OnItemEventListener onItemEventListener;

    public interface OnItemEventListener {
        void onBottomReached(int position);
        void onItemClicked(String id);
    }

    public DiscountsAdapter(List<Discount> mItemList, OnItemEventListener listener) {
        this.mItemList = mItemList;
        this.onItemEventListener = listener;
    }

    @Override
    public DiscountsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_discount, parent, false);

        return new DiscountsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DiscountsViewHolder holder, int position) {
        holder.bind(mItemList.get(position), onItemEventListener);

        if (position == mItemList.size() - 1/* && position % 10 == 9*/) {
            onItemEventListener.onBottomReached(position);
        }
    }

    // is called asynchronously
    public void datasetChanged(final List<Discount> dataset) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                mItemList.addAll(dataset);
                DiscountsAdapter.this
                        .notifyItemRangeChanged(mItemList.size() - dataset.size() - 1,
                                dataset.size());

            }
        });
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    public static class DiscountsViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPhoto;
        TextView tvTime;
        TextView tvTitle;
        TextView tvCount;
        TextView tvDescription;
        TextView tvPlaceTitle;

        public DiscountsViewHolder(View itemView) {
            super(itemView);

            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_back);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvCount = (TextView) itemView.findViewById(R.id.tv_count);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_desc);
            tvPlaceTitle = (TextView) itemView.findViewById(R.id.tv_place_title);
        }

        public void bind(final Discount discount, final OnItemEventListener listener) {
            Picasso.with(ivPhoto.getContext())
                .load(discount.getLogoUrl())
                .placeholder(ContextCompat.getDrawable(ivPhoto.getContext(), R.drawable.image_placeholder))
                .fit().centerCrop()
                .into(ivPhoto);
            tvTime.setText(discount.getTime());
            tvTitle.setText(discount.getTitle());
            tvDescription.setText(discount.getDescription());
            tvPlaceTitle.setText(discount.getPlaceTitle());

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(discount.getId());
                }
            });
        }
    }
}
