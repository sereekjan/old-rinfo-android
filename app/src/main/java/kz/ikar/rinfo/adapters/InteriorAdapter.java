package kz.ikar.rinfo.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.InteriorAdapter.InteriorViewHolder;
import kz.ikar.rinfo.helpers.CircleTransform;
import kz.ikar.rinfo.models.Event;

/**
 * Created by User on 07.02.2018.
 */

public class InteriorAdapter extends RecyclerView.Adapter<InteriorViewHolder> {
    List<String> mItemList;

    public InteriorAdapter(List<String> mItemList) {
        this.mItemList = mItemList;
    }

    @Override
    public InteriorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_interior, parent, false);

        return new InteriorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InteriorViewHolder holder, int position) {
        holder.bind(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    // is called asynchronously
    public void datasetUpdate(final String dataset) {
        mItemList.add(dataset);
    }

    public static class InteriorViewHolder extends ViewHolder {
        ImageView ivPic;

        public InteriorViewHolder(View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.iv_picture);
        }

        public void bind(String pic) {
            Picasso.with(ivPic.getContext())
                .load(pic)
                .resize(64, 64)
                .into(ivPic);
        }
    }
}
