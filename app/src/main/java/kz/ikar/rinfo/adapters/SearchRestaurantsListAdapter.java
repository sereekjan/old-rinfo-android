package kz.ikar.rinfo.adapters;

import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.activities.MainActivity;
import kz.ikar.rinfo.models.Restaurant;

/**
 * Created by User on 08.01.2018.
 */

public class SearchRestaurantsListAdapter extends RecyclerView.Adapter<SearchRestaurantsListAdapter.SearchRestaurantsListViewHolder> {
    private List<Restaurant> mItemList;
    OnItemEventListener onItemEventListener;

    public List<Restaurant> getmItemList() {
        return mItemList;
    }

    public interface OnItemEventListener {
        void onBottomReached(int position);
        void onItemClicked(String restaurantId, int position);
        void onItemFabClicked(Restaurant restaurant, FloatingActionButton fabLike, TextView tvLikes);
    }

    public SearchRestaurantsListAdapter(List<Restaurant> mItemList, OnItemEventListener listener) {
        this.mItemList = mItemList;
        this.onItemEventListener = listener;
    }

    @Override
    public SearchRestaurantsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new SearchRestaurantsListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchRestaurantsListViewHolder holder, int position) {
        holder.bind(mItemList.get(position), onItemEventListener, position);
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    // is called asynchronously
    public void datasetChanged(final List<Restaurant> dataset) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                mItemList.addAll(dataset);
                SearchRestaurantsListAdapter.this
                    .notifyItemRangeChanged(mItemList.size() - dataset.size() - 1, dataset.size());
            }
        });
    }

    public static class SearchRestaurantsListViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPhoto;
        TextView tvLocation;
        TextView tvAvgCost;
        TextView tvName;
        TextView tvTypes;
        TextView tvDescription;
        TextView tvLikes;
        FloatingActionButton fabLike;
        CardView cvDiscount;
        TextView tvDiscountTime;
        TextView tvDiscountValue;
        LinearLayout llBlockDescription;

        public SearchRestaurantsListViewHolder(View itemView) {
            super(itemView);

            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_back);
            tvLocation = (TextView) itemView.findViewById(R.id.tv_location);
            tvAvgCost = (TextView) itemView.findViewById(R.id.tv_avg_cost);
            fabLike = (FloatingActionButton) itemView.findViewById(R.id.fab_like);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvTypes = (TextView) itemView.findViewById(R.id.tv_types);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            tvLikes = (TextView) itemView.findViewById(R.id.tv_likes);
            cvDiscount = (CardView) itemView.findViewById(R.id.cv_discount);
            tvDiscountTime = (TextView) itemView.findViewById(R.id.tv_discount_time);
            tvDiscountValue = (TextView) itemView.findViewById(R.id.tv_discount_value);
            llBlockDescription = (LinearLayout) itemView.findViewById(R.id.ll_block_description);
        }

        public void bind(final Restaurant restaurant, final OnItemEventListener listener, final int position) {
            Picasso.with(ivPhoto.getContext())
                .load(restaurant.getLogoUrl())
                .placeholder(ContextCompat.getDrawable(ivPhoto.getContext(), R.drawable.image_placeholder))
                .fit().centerCrop()
                .into(ivPhoto);
            tvLocation.setText(restaurant.getLocation());
            tvAvgCost.setText(restaurant.getAvgCost());
            tvName.setText(restaurant.getName());
            tvTypes.setText(restaurant.getTypes());
            tvLikes.setText(String.valueOf(restaurant.getLikes()));
            if (restaurant.getDescription().equals("")) {
                llBlockDescription.setVisibility(View.GONE);
            } else {
                tvDescription.setText(restaurant.getDescription());
            }
            if (restaurant.getDiscountValue() == null || restaurant.getDiscountValue().isEmpty()) {
                cvDiscount.setVisibility(View.GONE);
            } else {
                tvDiscountTime.setText(restaurant.getDiscountTime());
                tvDiscountValue.setText(restaurant.getDiscountValue());
            }

            if (restaurant.isFavorite()) {
                fabLike.setImageDrawable(ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.favorite_filled));
            } else {
                fabLike.setImageDrawable(ContextCompat.getDrawable(fabLike.getContext(), R.mipmap.favourite));
            }

            fabLike.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemFabClicked(restaurant, fabLike, tvLikes);
                }
            });

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.firstPhoto = ((BitmapDrawable)ivPhoto.getDrawable()).getBitmap();
                    listener.onItemClicked(restaurant.getId(), position);
                }
            });
        }
    }
}
