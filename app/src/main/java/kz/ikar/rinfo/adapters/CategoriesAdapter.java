package kz.ikar.rinfo.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import kz.ikar.rinfo.R;
import kz.ikar.rinfo.models.Category;

/**
 * Created by User on 08.01.2018.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {
    private List<Category> mItemList;
    OnItemEventListener onItemEventListener;

    public interface OnItemEventListener {
        void onBottomReached(int position);
        void onItemClicked(String id);
    }

    public CategoriesAdapter(List<Category> mItemList, OnItemEventListener listener) {
        this.mItemList = mItemList;
        this.onItemEventListener = listener;
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);

        return new CategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, int position) {
        holder.bind(mItemList.get(position), onItemEventListener);

        if (position == mItemList.size() - 1 && position % 10 == 9) {
            onItemEventListener.onBottomReached(position);
        }
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    // is called asynchronously
    public void datasetChanged(final List<Category> dataset) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                mItemList.addAll(dataset);
            }
        });
    }

    public static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvCount;
        ImageView ivBackground;

        public CategoriesViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvCount = (TextView) itemView.findViewById(R.id.tv_count);
            ivBackground = (ImageView) itemView.findViewById(R.id.iv_back);
        }

        public void bind(final Category category, final OnItemEventListener listener) {
            tvName.setText(category.getName());
            tvCount.setText(category.getCount() + " ресторанов");
            Picasso.with(ivBackground.getContext())
                    .load(category.getLogoUrl())
                    .centerCrop()
                    .fit()
                    .into(ivBackground);

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(category.getId());
                }
            });
        }
    }
}
