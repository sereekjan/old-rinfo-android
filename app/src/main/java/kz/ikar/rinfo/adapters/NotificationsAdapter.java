package kz.ikar.rinfo.adapters;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.NotificationsAdapter.NotificationViewHolder;
import kz.ikar.rinfo.models.Notification;

/**
 * Created by User on 05.03.2018.
 */

public class NotificationsAdapter extends Adapter<NotificationViewHolder> {
    List<Notification> mItemList;

    public NotificationsAdapter(List<Notification> mItemList) {
        this.mItemList = mItemList;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_notification, parent, false);

        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        holder.bind(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    static class NotificationViewHolder extends ViewHolder {
        CircleImageView civPlaceImg;
        TextView tvText;
        TextView tvTime;

        public NotificationViewHolder(View itemView) {
            super(itemView);

            civPlaceImg = itemView.findViewById(R.id.civ_place_img);
            tvText = itemView.findViewById(R.id.tv_text);
            tvTime = itemView.findViewById(R.id.tv_time);
        }

        public void bind(Notification notification) {
            if (notification.getImgUrl() != null) {
                Picasso.with(civPlaceImg.getContext())
                    .load(notification.getImgUrl())
                    .resize(50, 50)
                    .centerCrop()
                    .into(civPlaceImg);
            }

            tvText.setText(notification.getText());

            tvTime.setText(notification.getTime());
        }
    }
}
