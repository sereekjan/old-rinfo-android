package kz.ikar.rinfo.adapters;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import kz.ikar.rinfo.R;
import kz.ikar.rinfo.adapters.FilterPricesAdapter.OnItemEventListener;
import kz.ikar.rinfo.adapters.TimeAdapter.TimeViewHolder;

/**
 * Created by User on 28.02.2018.
 */

public class TimeAdapter extends Adapter<TimeViewHolder> {
    List<String> mItemList;
    private OnItemEventListener listener;
    public int indexSelected = -1;

    public interface OnItemEventListener {
        void onItemClicked(String key);
    }

    public TimeAdapter(List<String> mItemList,
        OnItemEventListener listener) {
        this.mItemList = mItemList;
        this.listener = listener;
    }

    @Override
    public TimeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_time, parent, false);

        return new TimeViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(TimeViewHolder holder, final int position) {
        if (indexSelected != -1 && position == indexSelected) {
            holder.tvTime.setTextColor(Color.WHITE);
            holder.cvBack.setCardBackgroundColor(
                ContextCompat.getColor(holder.cvBack.getContext(), R.color.colorAccent));
        } else {
            holder.tvTime.setTextColor(
                ContextCompat.getColor(holder.tvTime.getContext(), R.color.colorTitle));
            holder.cvBack.setCardBackgroundColor(Color.WHITE);
        }
        holder.tvTime.setText(mItemList.get(position));

        holder.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == indexSelected) {
                    /*holder.tvPrice.setTextColor(
                        ContextCompat.getColor(holder.tvPrice.getContext(), R.color.colorTextDefault));
                    holder.cvBack.setBackgroundColor(
                        ContextCompat.getColor(holder.cvBack.getContext(), R.color.colorBackground2));
*/
                    indexSelected = -1;
                } else {
                    /*holder.tvPrice.setTextColor(Color.WHITE);
                    holder.cvBack.setBackgroundColor(
                        ContextCompat.getColor(holder.cvBack.getContext(), R.color.colorAccent));
*/
                    //int oldIndex = indexSelected;
                    indexSelected = position;
                }
                notifyDataSetChanged();
                listener.onItemClicked(mItemList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    static class TimeViewHolder extends ViewHolder {
        TextView tvTime;
        CardView cvBack;

        public TimeViewHolder(View itemView) {
            super(itemView);

            tvTime = itemView.findViewById(R.id.tv_time);
            cvBack = itemView.findViewById(R.id.cv_back);
        }
    }

}
